//
//  CombineVoidActionTests.swift
//  TheEyeLandNewsTests
//
//  Created by Zoltan Bognar The EyeLand on 23/03/2021.
//  Copyright © 2021 Mgr. Art. Ing. Zoltan Bognar. All rights reserved.
//

import XCTest
@testable import TheEyeLandNews
import Combine

class CombineVoidActionTests: XCTestCase {

    var action: Combine.Action<Void, String, NSError>!
    @Published var enabled: Bool = false
    lazy var cancellableSet: Set<AnyCancellable> = []

    var executionCount = 0
    var completedCount = 0
    var values: [String] = []
    var errors: [NSError] = []
    var runCompleted: (() -> Void)!

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        executionCount = 0
        completedCount = 0
        values = []
        errors = []

        action = Combine.Action<Void, String, NSError> { [unowned self] _ in

            self.executionCount += 1

            return MultiValuePublisher<String, NSError> { [unowned self] subject in
                subject.send("0")
                subject.send("00")
                self.runCompleted = {
                    subject.send(completion: Subscribers.Completion<NSError>.finished)
                }
            }.eraseToAnyPublisher()
        }

        action.values.sink { [unowned self] in self.values.append($0) }.store(in: &cancellableSet)
        action.errors.sink { [unowned self] in self.errors.append($0) }.store(in: &cancellableSet)
        action.completed.sink { [unowned self]  in self.completedCount += 1 }.store(in: &cancellableSet)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        cancellableSet.forEach {
            $0.cancel()
        }

        action = nil
    }

    func testVoidExecution() {
        XCTContext.runActivity(named: "should execute successfully") { _ in
            // given
            // when
            action.apply()
            // then
            XCTAssertEqual(executionCount, 1, "executionCount should be 1")
            XCTAssertEqual(action.isExecuting, true, "action.isExecuting should be true")
            XCTAssert(values == ["0", "00"], "values should have 2 values 0 and 00")
            XCTAssert(errors == [], "errors should be empty")

            // when
            self.runCompleted()
            // then
            XCTAssert(completedCount == 1, "completedCount should be 1")
            XCTAssertEqual(action.isExecuting, false, "action.isExecuting should be false")
            XCTAssert(values == ["0", "00"], "values should have 2 values 0 and 00")
            assertEmpty(errors, message: { _ in "errors should be empty"})
        }
    }

}
