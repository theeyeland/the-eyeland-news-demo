//
//  ObjectMappingAppNewsIneractorSpec.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 09.10.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import Quick
import Nimble
import Swinject
import ReactiveSwift

import Alamofire

@testable import TheEyeLandNews

class ObjectMappingAppNewsIneractorSpec: QuickSpec {

    func spec(withRouter router: TheEyeLandNews.Router) {
        describe("Interactor with REAL DataProvider with router \(router)") {

            var container: Container!

            beforeEach {
                container = Container()

                container.register(Session.self) { _ in
                    return Session(startRequestsImmediately: false)
                    }.inObjectScope(.container)

                container.register(NetworkingProtocol.self) { resolver in
                    let networking = Networking()
                    networking.afSession = resolver.resolve(Session.self)!
                    return networking
                }.inObjectScope(.transient)

                // DataProvider
                container.register(ObjectMappingAppNewsDataProvider.self) { resolver in
                    let rdp = RemoteDataCodableProvider<TheEyeLandNews.Router, ObjectMappingAppNewsModel>()
                    rdp.networking = resolver.resolve(NetworkingProtocol.self)!
                    return ObjectMappingAppNewsDataProvider(rdp)
                }

                // Interactor
                container.register(ObjectMappingAppNewsInteractorProtocol.self) { resolver in
                    let interactor = ObjectMappingAppNewsInteractor(dataProvider: resolver.resolve(ObjectMappingAppNewsDataProvider.self)!)
                    interactor.router = router
                    return interactor
                }.initCompleted { (_, interactor) in
                    interactor.initialize()
                }
            }

            it("expect completed called after fetch") {
                let interactor = container.resolve(ObjectMappingAppNewsInteractorProtocol.self)!
                var completed = false

                interactor.completed.observeResult {_ in
                    completed = true
                }
                expect(completed) == false

                interactor.fetch()

                expect(completed).toEventually(equal(true))
            }

            it("expect models not empty after fetch") {
                let interactor = container.resolve(ObjectMappingAppNewsInteractorProtocol.self)!

                var models: [ObjectMappingAppNewsModel] = []

                interactor.models.signal.observeResult({ (result) in
                    if case .success(let values) = result {
                        models = values
                    }
                })

                expect(models).to(beEmpty())

                interactor.fetch()

                expect(models).toEventuallyNot(haveCount(0))

                for model in models {
                    expect(model.imageLink).toEventuallyNot(beEmpty())
                }
            }

            it("expect model values are not empty after fetch") {
                let interactor = container.resolve(ObjectMappingAppNewsInteractorProtocol.self)!

                var models: [ObjectMappingAppNewsModel] = []

                interactor.models.signal.observeResult({ (result) in
                    if case .success(let values) = result {
                        models = values
                    }
                })

                expect(models).to(beEmpty())

                interactor.fetch()

                expect(models).toEventuallyNot(haveCount(0))

                for model in models {
                    expect(model.title).toEventuallyNot(beEmpty())
                    expect(model.appstoreId).toEventuallyNot(equal(0))
                    expect(model.imageLink).toEventuallyNot(beEmpty())
                }
            }

            it("expect models has unique ids after fetch") {
                let interactor = container.resolve(ObjectMappingAppNewsInteractorProtocol.self)!

                var models: [ObjectMappingAppNewsModel] = []
                var modelsSet = Set<Int>()

                interactor.models.signal.observeResult({ (result) in
                    if case .success(let values) = result {
                        models = values
                        modelsSet = Set<Int>(values.map({ (model) -> Int in
                            return model.newsId
                        }))
                    }
                })

                expect(models).to(beEmpty())
                expect(modelsSet).to(beEmpty())

                interactor.fetch()

                expect(models.count).toEventually(equal(modelsSet.count))
            }

            it("expect valid imageURLs in models after fetch") {
                let interactor = container.resolve(ObjectMappingAppNewsInteractorProtocol.self)!

                var models: [ObjectMappingAppNewsModel] = []

                interactor.models.signal.observeResult({ (result) in
                    if case .success(let values) = result {
                        models = values
                    }
                })

                expect(models).to(beEmpty())

                interactor.fetch()

                expect(models).toEventuallyNot(haveCount(0))

                for model in models {
                    var url: URL?
                    URL.storageURLProducer(relativePath: model.imageLink).startWithValues { (urlRespnse) in
                        url = urlRespnse
                    }

                    expect(url).toEventuallyNot(beNil())
                }
            }

            it("expect valid images after download") {
                let interactor = container.resolve(ObjectMappingAppNewsInteractorProtocol.self)!

                var models: [ObjectMappingAppNewsModel] = []

                interactor.models.signal.observeResult({ (result) in
                    if case .success(let values) = result {
                        models = values
                    }
                })

                expect(models).to(beEmpty())

                interactor.fetch()

                expect(models).toEventuallyNot(haveCount(0))

                for model in models {
                    var image: UIImage?
                    var url: URL?

                    URL.storageURLProducer(relativePath: model.imageLink).startWithValues { (urlRespnse) in
                        url = urlRespnse
                        if let url = url {
                            AF.request(url).responseImage(completionHandler: { (response) in
                                image = response.value
                            })
                        }
                    }

                    expect(image).toEventuallyNot(beNil(), description: "for App: \(model.title) download failed: \(url?.absoluteString ?? "")")
                }
            }
        }
    }

    override func spec() {

        beforeEach {
        }

        afterEach {
        }

        Nimble.AsyncDefaults.timeout = .seconds(5)
//        for router in TheEyeLandNews.Router.applicationRouters {
//            spec(withRouter: router)
//        }

        spec(withRouter: TheEyeLandNews.Router.starScalesPro)

    }
}
