//
//  AlwaysPublisherTest.swift
//  TheEyeLandNewsTests
//
//  Created by Zoltan Bognar The EyeLand on 05/03/2021.
//  Copyright © 2021 Mgr. Art. Ing. Zoltan Bognar. All rights reserved.
//

import XCTest
@testable import TheEyeLandNews
import Combine

class AlwaysPublisherTest: XCTestCase {

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testItEmitsASingleValue() {
        var output: [Int] = []
        _ = Always(1).first().print("First").sink { output.append($0) }
        XCTAssertEqual(output, [1])
    }

    func testSquareNumbers() {
        var output: [Double] = []

        let numbers = [1, 2, 3, 4].publisher

        let squares = numbers.zip(Always(2))
          .map { pow(Double($0), Double($1)) }

        _ = squares.sink { output.append($0)}

        XCTAssertEqual(output, [1, 4, 9, 16])
    }
}
