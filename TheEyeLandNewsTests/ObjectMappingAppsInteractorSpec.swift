//
//  ObjectMappingAppsInteractorSpec.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 02.10.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import Quick
import Nimble
import Swinject
import ReactiveSwift
import Combine

import AlamofireImage
import Alamofire

@testable import TheEyeLandNews

class DummyRemoteDataProvider<T: Decodable, R: NetworkingRouterProtocol>: BaseDataProvider, RemoteDataCodableProviderProtocol {

    var resolver: Resolver!

    var update: Action<R, Void, RemoteDataProviderError> = {
        return Action<R, Void, RemoteDataProviderError> { _ in
            return SignalProducer<Void, RemoteDataProviderError> { observer, _ in
                observer.sendCompleted()
            }
        }
    }()

    lazy var fetchObject: Action<R, T, RemoteDataProviderError> = {
        return Action<R, T, RemoteDataProviderError> { [unowned self] _ in
            return SignalProducer<T, RemoteDataProviderError> { [unowned self] observer, _ in

                let model1: T = self.resolver.resolve(T.self, name: "1")!

                observer.send(value: model1)
                observer.sendCompleted()
            }
        }
    }()

    lazy var fetch: Action<R, [T], RemoteDataProviderError>  = {
        return Action<R, [T], RemoteDataProviderError> { [unowned self] _ in
            return SignalProducer<[T], RemoteDataProviderError> { observer, _ in

                let model1: T = self.resolver.resolve(T.self, name: "1")!
                let model2: T = self.resolver.resolve(T.self, name: "2")!

                observer.send(value: [model1, model2])
                observer.sendCompleted()
            }
        }
    }()

    lazy var fetchCF: Combine.Action<R, [T], RemoteDataProviderError> = {
        return Combine.Action<R, [T], RemoteDataProviderError> { _ in
            return Future<[T], RemoteDataProviderError> { promise in
                let model1: T = self.resolver.resolve(T.self, name: "1")!
                let model2: T = self.resolver.resolve(T.self, name: "2")!

                return promise(.success([model1, model2]))
            }.eraseToAnyPublisher()
        }
    }()
}

class DummyErrorRemoteDataProvider<T: Decodable, R: NetworkingRouterProtocol>: BaseDataProvider, RemoteDataCodableProviderProtocol {

    lazy var update: Action<R, Void, RemoteDataProviderError> = {
        return Action<R, Void, RemoteDataProviderError> {  _ in
            return SignalProducer<Void, RemoteDataProviderError> { observer, _ in
                observer.send(error: RemoteDataProviderError.dataMappingFailed(apiPath: "Dummy path"))
            }
        }
    }()

    lazy var fetchObject: Action<R, T, RemoteDataProviderError> = {
        return Action<R, T, RemoteDataProviderError> {  _ in
            return SignalProducer<T, RemoteDataProviderError> { observer, _ in
                observer.send(error: RemoteDataProviderError.dataMappingFailed(apiPath: "Dummy path"))
            }
        }
    }()

    lazy var fetch: Action<R, [T], RemoteDataProviderError>  = {
        return Action<R, [T], RemoteDataProviderError> { _ in
            return SignalProducer<[T], RemoteDataProviderError> { observer, _ in

                observer.send(error: RemoteDataProviderError.dataMappingFailed(apiPath: "Dummy path"))
            }
        }
    }()

    lazy var fetchCF: Combine.Action<R, [T], RemoteDataProviderError> = {
        return Combine.Action<R, [T], RemoteDataProviderError> { _ in
            return Future<[T], RemoteDataProviderError> { promise in
                return promise(.failure(RemoteDataProviderError.dataMappingFailed(apiPath: "Dummy path")))
            }.eraseToAnyPublisher()
        }
    }()
}

class ObjectMappingAppsInteractorSpec: QuickSpec {

    func interactorWithRealDataProvider() {

        describe("Interactor with REAL DataProvider") {

            var container: Container!

            beforeEach {
                container = Container()

                container.register(Session.self) { _ in
                    return Session(startRequestsImmediately: false)
                    }.inObjectScope(.container)

                container.register(NetworkingProtocol.self) { resolver in
                    let networking = Networking()
                    networking.afSession = resolver.resolve(Session.self)!
                    return networking
                }.inObjectScope(.transient)

                // DataProvider
                container.register(ObjectMappingAppsDataProvider.self) { resolver in
                    let rdp = RemoteDataCodableProvider<TheEyeLandNews.Router, ObjectMappingAppsModel>()
                    rdp.networking = resolver.resolve(NetworkingProtocol.self)!
                    return ObjectMappingAppsDataProvider(rdp)
                }

                // Interactor
                container.register(ObjectMappingAppsInteractorProtocol.self) { resolver in
                    return ObjectMappingAppsInteractor(dataProvider: resolver.resolve(ObjectMappingAppsDataProvider.self)!)
                }.initCompleted { (_, interactor) in
                    interactor.initialize()
                }
            }

            it("expect completed called after fetch") {
                let interactor = container.resolve(ObjectMappingAppsInteractorProtocol.self)!
                var completed = false

                interactor.completed.observeResult {_ in
                   completed = true
                }
                expect(completed) == false

                interactor.fetch()

                expect(completed).toEventually(equal(true))
            }

            it("expect models not empty after fetch") {
                let interactor = container.resolve(ObjectMappingAppsInteractorProtocol.self)!

                var models: [ObjectMappingAppsModel] = []

                interactor.models.signal.observeResult({ (result) in
                    if case .success(let values) = result {
                        models = values
                    }
                })

                expect(models).to(beEmpty())

                interactor.fetch()

                expect(models).toEventuallyNot(haveCount(0))

                for model in models {
                    expect(model.title).toEventuallyNot(beEmpty())
                    expect(model.subtitle).toEventuallyNot(beEmpty())
                    expect(model.router).toEventuallyNot(beEmpty())
                    expect(model.imageLink).toEventuallyNot(beEmpty())
                }

            }

            it("expect models values are not empty after fetch") {
                let interactor = container.resolve(ObjectMappingAppsInteractorProtocol.self)!

                var models: [ObjectMappingAppsModel] = []

                interactor.models.signal.observeResult({ (result) in
                    if case .success(let values) = result {
                        models = values
                    }
                })

                expect(models).to(beEmpty())

                interactor.fetch()

                expect(models).toEventuallyNot(haveCount(0))

                for model in models {
                    expect(model.title).toEventuallyNot(beEmpty())
                    expect(model.subtitle).toEventuallyNot(beEmpty())
                    expect(model.router).toEventuallyNot(beEmpty())
                    expect(model.imageLink).toEventuallyNot(beEmpty())
                }
            }

            it("expect models has unique ids after fetch") {
                let interactor = container.resolve(ObjectMappingAppsInteractorProtocol.self)!

                var models: [ObjectMappingAppsModel] = []
                var modelsSet = Set<Int>()

                interactor.models.signal.observeResult({ (result) in
                    if case .success(let values) = result {
                        models = values
                        modelsSet = Set<Int>(values.map({ (model) -> Int in
                            return model.applicationId
                        }))
                    }
                })

                expect(models).to(beEmpty())

                interactor.fetch()

                expect(models.count).toEventually(equal(modelsSet.count))

            }

            it("expect valid imageURLs in models after fetch") {
                let interactor = container.resolve(ObjectMappingAppsInteractorProtocol.self)!

                var models: [ObjectMappingAppsModel] = []

                interactor.models.signal.observeResult({ (result) in
                    if case .success(let values) = result {
                        models = values
                    }
                })

                expect(models).to(beEmpty())

                interactor.fetch()

                expect(models).toEventuallyNot(haveCount(0))

                for model in models {
                    var url: URL?
                    URL.storageURLProducer(relativePath: model.imageLink).startWithValues { (urlRespnse) in
                        url = urlRespnse
                    }

                    expect(url).toEventuallyNot(beNil())
                }
            }

            it("expect valid images after download") {
                let interactor = container.resolve(ObjectMappingAppsInteractorProtocol.self)!

                var models: [ObjectMappingAppsModel] = []

                interactor.models.signal.observeResult({ (result) in
                    if case .success(let values) = result {
                        models = values
                    }
                })

                expect(models).to(beEmpty())

                interactor.fetch()

                expect(models).toEventuallyNot(haveCount(0))

                for model in models {
                    var image: UIImage?
                    var url: URL?

                    URL.storageURLProducer(relativePath: model.imageLink).startWithValues { (urlRespnse) in
                        url = urlRespnse
                        if let url = url {
                            AF.request(url).responseImage(completionHandler: { (response) in
                                image = response.value
                            })
                        }
                    }

                    expect(image).toEventuallyNot(beNil(), description: "for App: \(model.title) download failed: \(url?.absoluteString ?? "")")
                }
            }
        }
    }

    func interactorWithDummyDataProvider() {

        describe("Interactor with DUMMY DataProvider") {

            var container: Container!

            beforeEach {
                container = Container()

                // models for dummy test
                container.register(ObjectMappingAppsModel.self, name: "1") { _ in
                    let model = ObjectMappingAppsModel()
                    model.applicationId = 0
                    model.title = "Star Scales Pro"
                    model.router = "starScalesPro"
                    model.imageLink = "/images/starscalespro.jpg"
                    return model
                }

                container.register(ObjectMappingAppsModel.self, name: "2") { _ in
                    let model = ObjectMappingAppsModel()
                    model.applicationId = 1
                    model.title = "Star Scales HD"
                    model.router = "starScalesHD"
                    model.imageLink = "/images/starscaleshd.jpg"
                    return model
                }

                // Dummy dataProvider
                container.register(ObjectMappingAppsDataProvider.self) { resolver in
                    let dataProvider = DummyRemoteDataProvider<ObjectMappingAppsModel, TheEyeLandNews.Router>()
                    dataProvider.resolver = resolver
                    return ObjectMappingAppsDataProvider(dataProvider)

                }

                // Dummy Interactor
                container.register(ObjectMappingAppsInteractorProtocol.self) { resolver in
                    ObjectMappingAppsInteractor(dataProvider: resolver.resolve(ObjectMappingAppsDataProvider.self)!)
                }.initCompleted { (_, interactor) in
                    interactor.initialize()
                }
            }

            it("expect 2 models after fetch") {
                let dummyInteractor = container.resolve(ObjectMappingAppsInteractorProtocol.self)!

                var models: [ObjectMappingAppsModel] = []

                dummyInteractor.models.signal.observeResult({ (result) in
                    if case .success(let values) = result {
                        models = values
                    }
                })

                dummyInteractor.fetch()

                expect(models.count).toEventually(equal(2))
                if models.count > 1 {
                    expect(models[0].title).toEventually(equal("Star Scales Pro"))
                    expect(models[1].title).toEventually(equal("Star Scales HD"))
                }

                for model in models {
                    expect(model.title).toEventuallyNot(beEmpty())
                    expect(model.router).toEventuallyNot(beEmpty())
                    expect(model.imageLink).toEventuallyNot(beEmpty())
                }
            }

            it("expect models has unique ids after fetch") {
                let interactor = container.resolve(ObjectMappingAppsInteractorProtocol.self)!

                var models: [ObjectMappingAppsModel] = []
                // we leverige hashable protocol for ObjectMappingAppsModel
                var modelsSet = Set<ObjectMappingAppsModel>()

                interactor.models.signal.observeResult({ (result) in
                    if case .success(let values) = result {
                        models = values
                        modelsSet = Set<ObjectMappingAppsModel>(values)
                    }
                })

                expect(models).to(beEmpty())

                interactor.fetch()

                expect(modelsSet.count).toEventually(equal(models.count))

            }

            it("expect valid imageURLs in models after fetch") {
                let interactor = container.resolve(ObjectMappingAppsInteractorProtocol.self)!

                var models: [ObjectMappingAppsModel] = []

                interactor.models.signal.observeResult({ (result) in
                    if case .success(let values) = result {
                        models = values
                    }
                })

                expect(models).to(beEmpty())

                interactor.fetch()

                expect(models).toEventuallyNot(haveCount(0))

                for model in models {
                    var url: URL?
                    URL.storageURLProducer(relativePath: model.imageLink).startWithValues { (urlRespnse) in
                        url = urlRespnse
                    }

                    expect(url).toEventuallyNot(beNil())
                }
            }

            it("expect valid images after download") {
                let interactor = container.resolve(ObjectMappingAppsInteractorProtocol.self)!

                var models: [ObjectMappingAppsModel] = []

                interactor.models.signal.observeResult({ (result) in
                    if case .success(let values) = result {
                        models = values
                    }
                })

                expect(models).to(beEmpty())

                interactor.fetch()

                expect(models).toEventuallyNot(haveCount(0))

                for model in models {
                    var image: UIImage?
                    var url: URL?

                    URL.storageURLProducer(relativePath: model.imageLink).startWithValues { (urlRespnse) in
                        url = urlRespnse
                        if let url = url {
                            AF.request(url).responseImage(completionHandler: { (response) in
                                image = response.value
                            })
                        }
                    }

                    expect(image).toEventuallyNot(beNil(), description: "for App: \(model.title) download failed: \(url?.absoluteString ?? "")")
                }
            }
        }
    }

    func interactorWithDummyErrorDataProvider() {

        describe("Interactor with DUMMY Error DataProvider") {

            var container: Container!

            beforeEach {
                container = Container()

                // Dummy Error dataProvider
                container.register(ObjectMappingAppsDataProvider.self) { _ in
                    let dataProvider = DummyErrorRemoteDataProvider<ObjectMappingAppsModel, TheEyeLandNews.Router>()
                    return ObjectMappingAppsDataProvider(dataProvider)

                }

                // Dummy Error Interactor
                container.register(ObjectMappingAppsInteractorProtocol.self) { resolver in
                    ObjectMappingAppsInteractor(dataProvider: resolver.resolve(ObjectMappingAppsDataProvider.self)!)
                }.initCompleted { (_, interactor) in
                    interactor.initialize()
                }

            }

            it("expect error dataMappingFailed") {
                let dummyErrorInteractor = container.resolve(ObjectMappingAppsInteractorProtocol.self)!

                var dataProviderError: RemoteDataProviderError = .none

                dummyErrorInteractor.error.signal.observeResult({ (result) in
                    if case .success(let error) = result {
                        dataProviderError = error
                    }
                })

                dummyErrorInteractor.fetch()

                expect(dataProviderError).toEventually(equal(RemoteDataProviderError.dataMappingFailed(apiPath: "")))

                expect(dummyErrorInteractor.models.value.count).toEventually(equal(0))
            }
        }
    }

    override func spec() {

        Nimble.AsyncDefaults.timeout = .seconds(5)
        interactorWithRealDataProvider()
        interactorWithDummyDataProvider()
        interactorWithDummyErrorDataProvider()

    }
}
