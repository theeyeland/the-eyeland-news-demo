//
//  CombineActionTests.swift
//  TheEyeLandNewsTests
//
//  Created by Zoltan Bognar The EyeLand on 03/03/2021.
//  Copyright © 2021 Mgr. Art. Ing. Zoltan Bognar. All rights reserved.
//

import XCTest
@testable import TheEyeLandNews
import Combine

func assertEmpty<T>(
    _ collection: T,
    message: (T) -> String = { "Collection with \($0.count) elements is not empty" },
    file: StaticString = #filePath,
    line: UInt = #line
) where T: Collection {
    guard collection.isEmpty == false else { return }

    XCTFail(message(collection), file: file, line: line)
}

class CombineActionTests: XCTestCase {

    var action: Combine.Action<Int, String, NSError>!
    @Published var enabled: Bool = false
    lazy var cancellableSet: Set<AnyCancellable> = []

    var executionCount = 0
    var completedCount = 0
    var values: [String] = []
    var errors: [NSError] = []
    var runCompleted: (() -> Void)!
    var runError: (() -> Void)!

    let testError = NSError(domain: "CombineActionTest", code: 1, userInfo: nil)

    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
        executionCount = 0
        completedCount = 0
        values = []
        errors = []

        action = Combine.Action<Int, String, NSError> { [unowned self] number in

            self.executionCount += 1

            return MultiValuePublisher<String, NSError> { [unowned self] subject in
                if number % 2 == 0 {
                    subject.send("\(number)")
                    subject.send("\(number)\(number)")
                    self.runCompleted = {
                        subject.send(completion: Subscribers.Completion<NSError>.finished)
                    }
                } else {
                    self.runError = { [unowned self] in
                        subject.send(completion: Subscribers.Completion<NSError>.failure(self.testError))
                    }
                }
            }.eraseToAnyPublisher()
        }

        action.values.sink { [unowned self] in self.values.append($0) }.store(in: &cancellableSet)
        action.errors.sink { [unowned self] in self.errors.append($0) }.store(in: &cancellableSet)
        action.completed.sink { [unowned self]  in self.completedCount += 1 }.store(in: &cancellableSet)
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
        cancellableSet.forEach {
            $0.cancel()
        }

        action = nil
    }

    func testCompleted() {

        XCTContext.runActivity(named: "should send a value whenever the publisher completes") { _ in
            // given
            // when
            action.apply(value: 0)
            // then
            XCTAssertEqual(completedCount, 0, "completedCount should be 0")

            // when
            self.runCompleted()
            // then
            XCTAssertEqual(completedCount, 1, "completedCount should be 1")

            // when
            action.apply(value: 2)
            self.runCompleted()
            // then
            XCTAssertEqual(completedCount, 2, "completedCount should be 2")
        }

        XCTContext.runActivity(named: "should not send a value when the producer fails") { _ in
            // given
            completedCount = 0
            // when
            action.apply(value: 1)
            self.runError()
            // then
            XCTAssertEqual(completedCount, 0, "completedCount should be 0")
        }
    }

    func testExecution() {
        XCTContext.runActivity(named: "should execute successfully") { _ in
            // given
            // when
            action.apply(value: 0)
            // then
            XCTAssertEqual(executionCount, 1, "executionCount should be 1")
            XCTAssertEqual(action.isExecuting, true, "action.isExecuting should be true")
            XCTAssert(values == ["0", "00"], "values should have 2 values 0 and 00")
            XCTAssert(errors == [], "errors should be empty")

            // when
            self.runCompleted()
            // then
            XCTAssert(completedCount == 1, "completedCount should be 1")
            XCTAssertEqual(action.isExecuting, false, "action.isExecuting should be false")
            XCTAssert(values == ["0", "00"], "values should have 2 values 0 and 00")
            assertEmpty(errors, message: { _ in "errors should be empty"})
        }

        XCTContext.runActivity(named: "should execute with an error") { _ in
            // given
            executionCount = 0
            values = []
            // when
            action.apply(value: 1)
            // then
            XCTAssertEqual(executionCount, 1, "executionCount should be 1")
            XCTAssertEqual(action.isExecuting, true, "action.isExecuting should be true")
            XCTAssert(values == [], "values should have 0 values")
            // when
            self.runError()
            // then
            XCTAssertEqual(action.isExecuting, false, "action.isExecuting should be false")
            XCTAssert(errors.count == 1, "errors should be have 1 value")
            XCTAssertEqual(values, [], "values should be empty")
            XCTAssertEqual(errors, [self.testError], "errors should contain testError")
        }
    }
}
