//
//
//  NetworkManager.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 22.06.18.
//  Copyright © 2020 The EyeLand s.r.o. All rights reserved.
//

import Foundation
import Reachability
import ReactiveSwift

class NetworkManager: NSObject {

    var reachability: Reachability!

    static let sharedInstance: NetworkManager = { return NetworkManager() }()

    var reachibilityChanged: MutableProperty<Bool> = MutableProperty<Bool>(true)

    override init() {
        super.init()

        if let reachability = try? Reachability() {
            self.reachability = reachability
        } else {
            log.debug("‼️Unable to instantiate Reachability")
            return
        }

        NotificationCenter.default.addObserver(
            self,
            selector: #selector(networkStatusChanged(_:)),
            name: Notification.Name("reachabilityChanged"),
            object: reachability
        )

        do {
            try reachability.startNotifier()
        } catch {
            log.debug("Unable to start notifier")
        }
    }

    @objc func networkStatusChanged(_ notification: Notification) {
        // Do something globally here!
        let value = (NetworkManager.sharedInstance.reachability).connection != .unavailable
        if value != reachibilityChanged.value {
            reachibilityChanged.value = value
        }
    }

    static func stopNotifier() {
            NetworkManager.sharedInstance.reachability.stopNotifier()
    }

    static func isReachable(completed: @escaping (NetworkManager) -> Void) {
        if (NetworkManager.sharedInstance.reachability).connection != .unavailable {
            completed(NetworkManager.sharedInstance)
        }
    }

    static func isUnreachable(completed: @escaping (NetworkManager) -> Void) {
        if (NetworkManager.sharedInstance.reachability).connection == .unavailable {
            completed(NetworkManager.sharedInstance)
        }
    }

    static func isReachableViaWWAN(completed: @escaping (NetworkManager) -> Void) {
        if (NetworkManager.sharedInstance.reachability).connection == .cellular {
            completed(NetworkManager.sharedInstance)
        }
    }

    static func isReachableViaWiFi(completed: @escaping (NetworkManager) -> Void) {
        if (NetworkManager.sharedInstance.reachability).connection == .wifi {
            completed(NetworkManager.sharedInstance)
        }
    }
}
