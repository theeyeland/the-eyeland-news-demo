//
//  VCRefreshable.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 12.10.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import ReactiveSwift
import ReactiveCocoa
import Combine

protocol Refreshable {

    func refreshed() -> Action<(), Bool, Never>
    func refreshedCF() -> AnyPublisher<(), Never >
    func endRefreshing()
}
protocol TVCRefreshable: Refreshable {

    var tableView: UITableView! {get set}
}

extension TVCRefreshable {

    func refreshed() -> Action<(), Bool, Never> {
        let action = Action<(), Bool, Never> { _ in
            SignalProducer<Bool, Never> { observer, _ in
                observer.send(value: true)
                observer.sendCompleted()
            }
        }
        let rc = UIRefreshControl()
        rc.reactive.refresh = CocoaAction(action)
        tableView.refreshControl = rc
        return action
    }

    func refreshedCF() -> AnyPublisher<(), Never> {
        let rc = UIRefreshControl()
        let publisher = rc.publisher(for: .valueChanged).map { _ in return () }
        tableView.refreshControl = rc
        return publisher.eraseToAnyPublisher()
    }

    func endRefreshing() {
        if let rc = tableView.refreshControl {
            rc.endRefreshing()
        }
    }
}

protocol CVCRefreshable: Refreshable {

    var collectionView: UICollectionView! {get set}
}

extension CVCRefreshable {

    func refreshed() -> Action<(), Bool, Never> {
        let action = Action<(), Bool, Never> { _ in
            SignalProducer<Bool, Never> { observer, _ in
                observer.send(value: true)
                observer.sendCompleted()
            }
        }
        let rc = UIRefreshControl()
        rc.reactive.refresh = CocoaAction(action)
        collectionView.refreshControl = rc
        return action
    }

    func refreshedCF() -> AnyPublisher<(), Never> {
        let rc = UIRefreshControl()
        let publisher = rc.publisher(for: .valueChanged).map { _ in return () }
        collectionView.refreshControl = rc
        return publisher.eraseToAnyPublisher()
    }

    func endRefreshing() {
        if let rc = collectionView.refreshControl {
            rc.endRefreshing()
        }
    }
}
