//
//  Networking.swift
//  Slovak Lines
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 06.09.18.
//  Copyright © 2020 The EyeLand s.r.o. All rights reserved.
//

import Foundation
import ReactiveSwift
import Alamofire
import AlamofireNetworkActivityIndicator
import Combine

func iso8601FullJSONDecoder() -> JSONDecoder {
    let decoder = JSONDecoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        decoder.dateDecodingStrategy = .formatted(DateFormatter.iso8601Full)
    }
    return decoder
}

func iso8601FullJSONEncoder() -> JSONEncoder {
    let encoder = JSONEncoder()
    if #available(iOS 10.0, OSX 10.12, tvOS 10.0, watchOS 3.0, *) {
        encoder.dateEncodingStrategy = .formatted(DateFormatter.iso8601Full)
    }
    return encoder
}

enum DownloadResponseFormat {
    case none
    case downloadStarted
    case progress(Progress)
    case url(URL?)
}

func == (lhs: DownloadResponseFormat, rhs: DownloadResponseFormat) -> Bool {
    switch (lhs, rhs) {
    case (.downloadStarted, .downloadStarted): return true
    case (.progress, .progress): return true
    case (.url, .url): return true
    default: return false
    }
}

public enum NetworkingAuthentification {
    case none
    case header(userName: String, password: String)
    case basicHeaderCredentials(userName: String, password: String)
    case credential(credential: URLCredential)
    case accessToken(token: String)
}

/// `KeyPathDataPreprocessor` that returns json passed `Data`  with json data one level down refered with keyPath
///  e. g. when input json data looks like this///
///  ```
///  {
///     "keyPath":  [ { "a" = 1 },  { "a" = 0} ]
///  }
/// ```
/// returned json data wil look like this
///  ```
///  {
///     [ { "a" = 1 },  { "a" = 0} ]
///  }
/// ```
public struct KeyPathJSONDataPreprocessor: DataPreprocessor {

    private let keyPath: String?
    public init(keyPath: String?) {
        self.keyPath = keyPath
    }

    public func preprocess(_ data: Data) throws -> Data {

        guard let keyPath = keyPath, !keyPath.isEmpty else { return data }

        // make sure this JSON is in the format we expect
        guard let result = try? JSONSerialization.jsonObject(with: data, options: []) as? [String: Any] else {
            return data
        }

        if let JSON = (result as AnyObject?)?.value(forKeyPath: keyPath) {
            if let responseData = try? JSONSerialization.data(withJSONObject: JSON) {
                return responseData
            }
        }

        return data
    }
}

protocol NetworkingProtocol {

    /// update call without mapping
    func update<Input: NetworkingRouterProtocol>(withRouter router: Input) -> SignalProducer<Void, RemoteDataProviderError>
    /// fetch object which confirms to Decodable protocol
    func fetchObject<Output: Decodable, Input: NetworkingRouterProtocol>(withRouter router: Input) -> SignalProducer<Output, RemoteDataProviderError>
    /// fetch array of objects which confirms to Decodable protocol
    func fetchArray<Output: Decodable, Input: NetworkingRouterProtocol>(withRouter router: Input) -> SignalProducer<[Output], RemoteDataProviderError>
    /// fetch simple json
    func fetchJSON<Input: NetworkingRouterProtocol>(withRouter router: Input) -> SignalProducer<[[String: Any]], RemoteDataProviderError>
    /// download file - here pdf or apple wallet
    func download(toURL: URL, withRouter router: NetworkingRouterProtocol) -> SignalProducer<DownloadResponseFormat, RemoteDataProviderError>
    /// cancel ongoing file download
    func cancelDownloadRequest()

    /// fetch array of objects which confirms to Decodable protocol using Combine Framework
    func fetchArrayCF<Output: Decodable, Input: NetworkingRouterProtocol>(withRouter router: Input) -> AnyPublisher<[Output], RemoteDataProviderError>
}

class Networking: NetworkingProtocol, BaseDebugDeallocProtocol {

    private weak var downloadRequest: DownloadRequest?

    var afSession: Session!

    deinit {
        log.theeyeland.debugDealloc(withObject: self)
    }

    static func initialize() {
        NetworkActivityIndicatorManager.shared.isEnabled = true
    }

    func update<Input: NetworkingRouterProtocol>(withRouter router: Input) -> SignalProducer<Void, RemoteDataProviderError> {
        return NetworkingRequestAdapter(withRouter: router).urlConvertableProducer()
            .flatMap(FlattenStrategy.latest) { [unowned self] urlRequest -> SignalProducer<Void, RemoteDataProviderError> in
                return self.updateProducer(urlRequest: urlRequest, withRouter: router)
            }
    }

    func fetchObject<Output: Decodable, Input: NetworkingRouterProtocol>(withRouter router: Input) -> SignalProducer<Output, RemoteDataProviderError> {
        return NetworkingRequestAdapter(withRouter: router).urlConvertableProducer()
            .flatMap(FlattenStrategy.latest) { [unowned self] urlRequest -> SignalProducer<Output, RemoteDataProviderError> in
                return self.fetchObjectProducer(urlRequest: urlRequest, withRouter: router)
            }
    }

    func fetchArray<Output: Decodable, Input: NetworkingRouterProtocol>(withRouter router: Input) -> SignalProducer<[Output], RemoteDataProviderError> {
        return NetworkingRequestAdapter(withRouter: router).urlConvertableProducer()
            .flatMap(FlattenStrategy.latest) { [unowned self] urlRequest -> SignalProducer<[Output], RemoteDataProviderError> in
                return self.fetchArrayProducer(urlRequest: urlRequest, router: router)
            }
    }

    func fetchJSON<Input: NetworkingRouterProtocol>(withRouter router: Input) -> SignalProducer<[[String: Any]], RemoteDataProviderError> {
        return NetworkingRequestAdapter(withRouter: router).urlConvertableProducer()
            .flatMap(FlattenStrategy.latest) { [unowned self] urlRequest -> SignalProducer<[[String: Any]], RemoteDataProviderError> in
                return self.fetchJSONProducer(urlRequest: urlRequest, withRouter: router)
            }
    }

    func download(toURL: URL, withRouter router: NetworkingRouterProtocol) -> SignalProducer<DownloadResponseFormat, RemoteDataProviderError> {
        return NetworkingRequestAdapter(withRouter: router).urlConvertableProducer()
            .flatMap(FlattenStrategy.latest) { [unowned self] urlRequest -> SignalProducer<DownloadResponseFormat, RemoteDataProviderError> in
                return self.downloadProducer(urlRequest: urlRequest, toURL: toURL, withRouter: router)
            }
    }

    func cancelDownloadRequest() {
        if let request = downloadRequest {
            request.cancel()
        }
    }

    func fetchArrayCF<Output: Decodable, Input: NetworkingRouterProtocol>(withRouter router: Input) -> AnyPublisher<[Output], RemoteDataProviderError> {
        return NetworkingRequestAdapter(withRouter: router).urlConvertableCF()
            .flatMap { urlRequest -> AnyPublisher<[Output], RemoteDataProviderError> in
                return self.fetchArrayCFProducer(urlRequest: urlRequest, withRouter: router)
            }.eraseToAnyPublisher()
    }
}

extension Networking {

    private func addAuthentification<Input: NetworkingRouterProtocol>(fromRouter router: Input, forRequest request: DataRequest ) {
        if case .basicHeaderCredentials(let name, let password) = router.authentification {
            request.authenticate(username: name, password: password)
        } else if case .credential(let credential) = router.authentification {
            request.authenticate(with: credential)
        }
    }

    private func updateProducer<Input: NetworkingRouterProtocol>(urlRequest: URLRequest, withRouter router: Input) -> SignalProducer<Void, RemoteDataProviderError> {
        return SignalProducer<Void, RemoteDataProviderError> { [unowned self] observer, _ in

            let request = self.afSession.request(urlRequest)

            self.addAuthentification(fromRouter: router, forRequest: request)

            request.response { (response) in

                if let statusCode = response.response?.statusCode,
                    statusCode >= StatusCodesFormat.serverError.rawValue && statusCode < StatusCodesFormat.noInternetConnection.rawValue {
                    let localizedDescription = response.error?.localizedDescription ?? RemoteDataProviderError.defaultDescription
                    let statusCode = response.response?.statusCode ?? StatusCodesFormat.defaultStatusCode
                    let data = response.data ?? Data()

                    observer.send(error: RemoteDataProviderError.dataRequestFailed(description: localizedDescription,
                                                                                   statusCode: statusCode,
                                                                                   data: data,
                                                                                   apiPath: router.path))
                } else {
//                    log.debug("Request: \(String(describing: response.request))")   // original url request
//                    log.debug("Response: \(String(describing: response.response))") // http url response
                    observer.sendCompleted()
                }
            }

            request.resume()

        }
    }

    private func fetchObjectProducer<Output: Decodable, Input: NetworkingRouterProtocol>(urlRequest: URLRequest, withRouter router: Input) -> SignalProducer<Output, RemoteDataProviderError> {

        return SignalProducer<Output, RemoteDataProviderError> { [unowned self] observer, _ in

            let request = self.afSession.request(urlRequest)

            self.addAuthentification(fromRouter: router, forRequest: request)

            request.validate().responseDecodable(of: Output.self, dataPreprocessor: KeyPathJSONDataPreprocessor(keyPath: router.keypath), decoder: iso8601FullJSONDecoder()) { response  in

                if let error = response.error {
                    let localizedDescription = error.localizedDescription
                    let statusCode = response.response?.statusCode ?? StatusCodesFormat.defaultStatusCode
                    let data = response.data ?? Data()

                    observer.send(error: RemoteDataProviderError.dataRequestFailed(description: localizedDescription,
                                                                                   statusCode: statusCode,
                                                                                   data: data,
                                                                                   apiPath: router.path))
                }

                //                log.debug("Request: \(String(describing: response.request))")   // original url request
                //                log.debug("Response: \(String(describing: response.response))") // http url response
                //                log.debug("Result: \(response.result)")                         // response serialization result

                let models: Output? = response.value

                if let models = models {
                    observer.send(value: models)
                    observer.sendCompleted()
                } else {
                    observer.send(error: RemoteDataProviderError.dataMappingFailed(apiPath: router.path))
                }
            }

            request.resume()

        }
    }

    private func fetchArrayProducer<Output: Decodable, Input: NetworkingRouterProtocol>(urlRequest: URLRequest, router: Input) -> SignalProducer<[Output], RemoteDataProviderError> {

        return SignalProducer<[Output], RemoteDataProviderError> { [unowned self] observer, _ in
            let request = self.afSession.request(urlRequest)

            self.addAuthentification(fromRouter: router, forRequest: request)

            request.validate().responseDecodable(of: Array<Output>.self, dataPreprocessor: KeyPathJSONDataPreprocessor(keyPath: router.keypath), decoder: iso8601FullJSONDecoder()) { response  in

                if let error = response.error {
                    let localizedDescription = error.localizedDescription
                    let statusCode = response.response?.statusCode ?? StatusCodesFormat.defaultStatusCode
                    let data = response.data ?? Data()

                    observer.send(error: RemoteDataProviderError.dataRequestFailed(description: localizedDescription,
                                                                                   statusCode: statusCode,
                                                                                   data: data,
                                                                                   apiPath: router.path))
                }

                let models: [Output]? = response.value

                if let models = models {
                    observer.send(value: models)
                    observer.sendCompleted()
                } else {
                    observer.send(error: RemoteDataProviderError.dataMappingFailed(apiPath: router.path))
                }
            }

            request.resume()
        }
    }

    private func fetchJSONProducer<Input: NetworkingRouterProtocol>(urlRequest: URLRequest, withRouter router: Input) -> SignalProducer<[[String: Any]], RemoteDataProviderError> {
        return SignalProducer<[[String: Any]], RemoteDataProviderError> { [unowned self] observer, _ in

            let request = self.afSession.request(urlRequest)

            self.addAuthentification(fromRouter: router, forRequest: request)

            request.responseJSON { response in

                if let _ = response.error {
                    let localizedDescription = response.error?.localizedDescription ?? RemoteDataProviderError.defaultDescription
                    let statusCode = response.response?.statusCode ?? StatusCodesFormat.defaultStatusCode
                    let data = response.data ?? Data()
                    observer.send(error: RemoteDataProviderError.dataRequestFailed(description: localizedDescription,
                                                                                   statusCode: statusCode,
                                                                                   data: data,
                                                                                   apiPath: router.path))
                }

//                log.debug("Request: \(String(describing: response.request))")   // original url request
//                log.debug("Response: \(String(describing: response.response))") // http url response
//                log.debug("Result: \(response.result)")                         // response serialization result

                if let json = response.value {

                    if !router.keypath.isEmpty {
                        if let json = json as? [String: Any], let valuesArray = json[router.keypath] as? [[String: Any]] {
                            observer.send(value: valuesArray)
                            observer.sendCompleted()
                        } else {
                            let description = RemoteDataProviderError.jsonCastFailedDescription
                            observer.send(error: RemoteDataProviderError.dataRequestFailed(description: description,
                                                                                           statusCode: response.response?.statusCode ?? StatusCodesFormat.defaultStatusCode,
                                                                                           data: response.data ?? Data(),
                                                                                           apiPath: router.path))
                        }
                    } else {
                        if let valuesArray = json as? [[String: Any]] {
                            observer.send(value: valuesArray)
                            observer.sendCompleted()
                        } else {
                            let description = RemoteDataProviderError.jsonCastFailedDescription
                            observer.send(error: RemoteDataProviderError.dataRequestFailed(description: description,
                                                                                           statusCode: response.response?.statusCode ?? StatusCodesFormat.defaultStatusCode,
                                                                                           data: response.data ?? Data(),
                                                                                           apiPath: router.path))
                        }
                    }

                } else {
                    let description = RemoteDataProviderError.failedToGetJSONDescription
                    observer.send(error: RemoteDataProviderError.dataRequestFailed(description: description,
                                                                                   statusCode: StatusCodesFormat.defaultStatusCode,
                                                                                   data: Data(),
                                                                                   apiPath: router.path))
                }
            }

            request.resume()
        }
    }

    private func downloadProducer(urlRequest: URLRequest, toURL: URL, withRouter router: NetworkingRouterProtocol) -> SignalProducer<DownloadResponseFormat, RemoteDataProviderError> {
        return SignalProducer<DownloadResponseFormat, RemoteDataProviderError> { [unowned self] observer, _ in
            let destination: DownloadRequest.Destination = { _, _ in
                return (toURL, [.removePreviousFile, .createIntermediateDirectories])
            }

            observer.send(value: .downloadStarted)
            let request = self.afSession.download(urlRequest, to: destination)
                .downloadProgress { (progress) in
                    log.debug("download progress: \(progress)")
                    observer.send(value: .progress(progress))
                }
                .responseData { (response) in
                    if let error = response.error {
                        log.debug("download error: \(error)")
                        let localizedDescription = response.error?.localizedDescription ?? RemoteDataProviderError.defaultDescription
                        let statusCode = response.response?.statusCode ?? StatusCodesFormat.defaultStatusCode
                        observer.send(error: .dataRequestFailed(description: localizedDescription, statusCode: statusCode, data: Data(), apiPath: router.path))
                    } else if let response = response.response, response.statusCode == StatusCodesFormat.notFound.rawValue {
                        let statusCode = response.statusCode
                        observer.send(error: .dataDownloadFailed(description: "", statusCode: statusCode, data: Data(), apiPath: router.path))
                    } else {
                        log.debug("download finished")
                        observer.send(value: .url(toURL))
                        observer.sendCompleted()
                    }
            }

            self.downloadRequest = request
            request.resume()
        }
    }

    private func fetchArrayCFProducer<Output: Decodable, Input: NetworkingRouterProtocol>(urlRequest: URLRequest, withRouter router: Input) -> AnyPublisher<[Output], RemoteDataProviderError> {
        return Future<[Output], RemoteDataProviderError> { [unowned self] promise in
            let request = self.afSession.request(urlRequest)

            self.addAuthentification(fromRouter: router, forRequest: request)

            request.validate().responseDecodable(of: Array<Output>.self, dataPreprocessor: KeyPathJSONDataPreprocessor(keyPath: router.keypath), decoder: iso8601FullJSONDecoder()) { response  in

                if let error = response.error {
                    let localizedDescription = error.localizedDescription
                    let statusCode = response.response?.statusCode ?? StatusCodesFormat.defaultStatusCode
                    let data = response.data ?? Data()

                    promise(.failure(RemoteDataProviderError.dataRequestFailed(description: localizedDescription,
                                                                                   statusCode: statusCode,
                                                                                   data: data,
                                                                                   apiPath: router.path)))
                }

                let models: [Output]? = response.value

                if let models = models {
                    promise(.success(models))
                } else {
                    promise(.failure(RemoteDataProviderError.dataMappingFailed(apiPath: router.path)))
                }
            }

            request.resume()
        }.eraseToAnyPublisher()
    }
}
