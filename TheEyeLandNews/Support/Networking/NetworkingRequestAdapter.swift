//
//  NetworkingRequestAdapter.swift
//  Slovak Lines
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 18.09.18.
//  Copyright © 2020 The EyeLand s.r.o. All rights reserved.
//

import Foundation
import ReactiveSwift
import Alamofire
import FirebaseStorage
import Combine
import Swinject

public enum URLFormat {
    case none
    case remoteStorege
}

protocol NetworkingRouterProtocol {

    var authentification: NetworkingAuthentification {get}
    var method: String {get}
    var path: String {get}
    var keypath: String {get}
    var jsonEncoding: Bool {get}
    var languageCode: String {get}
    var parameters: [String: Any] {get}
    var urlFormat: URLFormat {get}
}

struct NetworkingRequestAdapter /*: URLRequestConvertible*/ {

    var router: NetworkingRouterProtocol
    var assembler: Assembler = Assembler.sharedAssembler

    init(withRouter router: NetworkingRouterProtocol) {
        self.router = router
    }
}

extension NetworkingRequestAdapter {

    private func getURLRequest(url: URL) throws -> URLRequest {
        var request = URLRequest(url: url)

        request.httpMethod = router.method

        if case .header(let name, let password) = router.authentification {
             let headerTuple = HTTPHeader.authorization(username: name, password: password)
            request.addValue(headerTuple.value, forHTTPHeaderField: headerTuple.name)
        } else if case .accessToken(let token) = router.authentification {
            request.addValue(token, forHTTPHeaderField: "Authorization")
        }

        request.addValue(router.languageCode, forHTTPHeaderField: "Accept-Language")
        request.addValue("ios", forHTTPHeaderField: "X-Device-Type")
        log.debug("⚙️ \(request.allHTTPHeaderFields ?? [:])")

        if router.jsonEncoding {
            return try JSONEncoding.default.encode(request, with: router.parameters)
        }

        let parameters = router.parameters

        if parameters.isEmpty {
            return request
        } else {
            return try URLEncoding.default.encode(request, with: parameters)
        }
    }

    private func mapToRequest(url: URL?) -> SignalProducer<URLRequest, RemoteDataProviderError> {
        if let url = url, let request = try? getURLRequest(url: url) {
            log.debug("urlConvertableProducer() \(url)")
            return SignalProducer(value: request)
        } else {
            return SignalProducer(error: RemoteDataProviderError.dataRequestFailed(description: RemoteDataProviderError.createRequestFaildDescription,
                                                                                   statusCode: 0,
                                                                                   data: Data(),
                                                                                   apiPath: router.path))
        }
    }

    private func mapToRequestCF(url: URL?) -> AnyPublisher<URLRequest, RemoteDataProviderError> {
        if let url = url, let request = try? getURLRequest(url: url) {
            log.debug("urlConvertableProducer() \(url)")
            return Future<URLRequest, RemoteDataProviderError> { promises in
                return promises(.success(request))
            }.eraseToAnyPublisher()
        } else {
            return Future<URLRequest, RemoteDataProviderError> { promises in
                let error = RemoteDataProviderError.dataRequestFailed(description: RemoteDataProviderError.createRequestFaildDescription,
                                                                      statusCode: 0,
                                                                      data: Data(),
                                                                      apiPath: router.path)
                return promises(.failure(error))
            }.eraseToAnyPublisher()
        }
    }

    public func urlConvertableProducer() -> SignalProducer<URLRequest, RemoteDataProviderError> {
        return URL.storageURLProducer(relativePath: router.path).flatMap(.latest) { (url) -> SignalProducer<URLRequest, RemoteDataProviderError> in
            return mapToRequest(url: url)
        }
    }

    public func urlConvertableCF() -> AnyPublisher<URLRequest, RemoteDataProviderError> {
        return URL.storageURLProducerCF(relativePath: router.path)
            .setFailureType(to: RemoteDataProviderError.self)
            .flatMap { (url) -> AnyPublisher<URLRequest, RemoteDataProviderError> in
            return mapToRequestCF(url: url)
        }.eraseToAnyPublisher()
    }
}
