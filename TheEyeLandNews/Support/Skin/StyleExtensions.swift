//
//  StyleExtensions.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 11.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit

public protocol Skin {

    func appearance()

    var mainColor: UIColor {get}

    var titleFont: UIFont {get}
    var titleColor: UIColor {get}
    var subTitleFont: UIFont {get}
    var subTitleColor: UIColor {get}

    // texts
    var normalTextFont: UIFont {get}
    var normalTextBoldFont: UIFont {get}
    var normalTextColor: UIColor {get}
    var normalTextLightColor: UIColor {get}
    var normalSmallTextFont: UIFont {get}

    // backgrounds
    var mainBackgroundColor: UIColor {get}
    var navigationBackgroundColor: UIColor {get}

    var navigationBarFont: UIFont {get}
}

public class SkinStore {

    public var skin: Skin?
    public static let sharedInstance = SkinStore()
}

public protocol StyleExtensionsProvider: class {}

extension StyleExtensionsProvider {

    public var style: Style<Self> {
        return Style(self)
    }

    public var customStyle: Style<Self> {
        return Style(self)
    }

    public static var style: Style<Self>.Type {
        return Style<Self>.self
    }

    public static var cutomStyle: Style<Self>.Type {
        return Style<Self>.self
    }
}

public struct Style<Base> {

    public let base: Base

    public var skin: Skin {
        guard let skin = SkinStore.sharedInstance.skin else { fatalError("No skin available!") }
        return skin
    }

    internal init(_ base: Base) {
        self.base = base
    }
}

extension Skin {

    func appearance() {
        UINavigationBar.appearance().style.main()
        UIBarButtonItem.appearance().style.main()
        UITabBarItem.appearance().style.main()
        UITabBar.appearance().style.main()
    }
}

extension UIView: StyleExtensionsProvider {}

extension Style where Base: UIView {

    public func main() {
        base.backgroundColor =  skin.mainBackgroundColor
    }

    public func imageGradient() {
        base.layer.cornerRadius = 10.0
        base.layer.masksToBounds = true
    }
}

extension Style where Base: UILabel {

    public func title() {
        base.font = skin.titleFont
        base.textColor = skin.titleColor
    }

    public func subTitle() {
        base.font = skin.subTitleFont
        base.textColor = skin.subTitleColor
    }

    public func normal() {
        base.font = skin.normalTextFont
        base.textColor = skin.normalTextColor
    }

    public func normalSmall() {
        base.font = skin.normalSmallTextFont
        base.textColor = skin.normalTextColor
    }
}

extension Style where Base: UIImageView {

    public func tableCellApp() {
        base.layer.cornerRadius = 10.0
        base.layer.masksToBounds = true
    }
    public func collectionCellApp() {
        base.layer.cornerRadius = 10.0
        base.layer.masksToBounds = true
    }
}

extension Style where Base: UINavigationBar {

    public func main() {
        base.tintColor = skin.normalTextColor
        base.titleTextAttributes = [NSAttributedString.Key.foregroundColor: skin.mainColor, NSAttributedString.Key.font: skin.navigationBarFont]
    }
}

extension UIBarButtonItem: StyleExtensionsProvider {}

extension Style where Base: UIBarButtonItem {

    public func main() {
        base.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: skin.normalTextColor, NSAttributedString.Key.font: skin.normalTextFont], for: UIControl.State.normal)
    }
}

extension UITabBarItem: StyleExtensionsProvider {}

extension Style where Base: UITabBarItem {

    public func main() {
        base.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: skin.normalTextColor, NSAttributedString.Key.font: skin.normalSmallTextFont], for: UIControl.State.normal)
        base.setTitleTextAttributes([NSAttributedString.Key.foregroundColor: skin.mainColor, NSAttributedString.Key.font: skin.normalSmallTextFont], for: UIControl.State.selected)
    }
}

extension Style where Base: UITabBar {

    public func main() {
        base.tintColor = skin.normalTextColor
    }
}
