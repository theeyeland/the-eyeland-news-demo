//
//  Enums.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 10.04.18.
//  Copyright © 2020 The EyeLand s.r.o. All rights reserved.
//

import Foundation

public enum DebugDeallocFormat {
    case none
    case common
    case assembly
    case viewModel
    case viewController
    case viewControllerCell
    case view
    case interactor
    case coordinator
    case dataProvider

    func tag() -> String {
        switch self {
        case .common:
            return Constant.Dealloc.Common
        case .assembly:
            return Constant.Dealloc.Assembly
        case .viewModel:
            return Constant.Dealloc.ViewModel
        case .viewController:
            return Constant.Dealloc.ViewController
        case .viewControllerCell:
            return Constant.Dealloc.ViewControllerCell
        case .view:
            return Constant.Dealloc.View
        case .interactor:
            return Constant.Dealloc.Interactor
        case .coordinator:
            return Constant.Dealloc.Coordinator
        case .dataProvider:
            return Constant.Dealloc.DataProvider
        default:
            return ""
        }
    }

    func preffix() -> String {
        switch self {
        case .common:
            return "✅ OBJ"
        case .assembly:
            return "✅ ASM"
        case .viewModel:
            return "✅ VM"
        case .viewController:
            return "✅ VC"
        case .viewControllerCell:
            return "✅ VCC"
        case .view:
            return "✅ VIEW"
        case .interactor:
            return "✅ INTER"
        case .coordinator:
            return "✅ COORD"
        case .dataProvider:
            return "✅ DPROV"
        default:
            return ""
        }
    }

}

public struct DebugDeallocOptions: OptionSet {

    public let rawValue: Int

    public init(rawValue: Int) {
        self.rawValue = rawValue
    }

    public static let none = DebugDeallocOptions(rawValue: 1 << 0)
    public static let common = DebugDeallocOptions(rawValue: 1 << 1)
    public static let assembly = DebugDeallocOptions(rawValue: 1 << 2)
    public static let viewModel = DebugDeallocOptions(rawValue: 1 << 3)
    public static let viewController = DebugDeallocOptions(rawValue: 1 << 4)
    public static let viewControllerCell = DebugDeallocOptions(rawValue: 1 << 5)
    public static let view = DebugDeallocOptions(rawValue: 1 << 6)
    public static let coordinator = DebugDeallocOptions(rawValue: 1 << 7)
    public static let interactor = DebugDeallocOptions(rawValue: 1 << 8)
    public static let dataProvider = DebugDeallocOptions(rawValue: 1 << 9)

    static let mvvmc: DebugDeallocOptions = [.common, .assembly, .viewModel, .viewController, .viewControllerCell, .coordinator, .interactor, .dataProvider]
    static let objects: DebugDeallocOptions = [.common]
    static let views: DebugDeallocOptions = [.view]

    func toStrings() -> [String] {

        var filter = [String]()

        if self.contains(.common) {
            filter.append(DebugDeallocFormat.common.tag())
        }
        if self.contains(.assembly) {
            filter.append(DebugDeallocFormat.assembly.tag())
        }
        if self.contains(.viewModel) {
            filter.append(DebugDeallocFormat.viewModel.tag())
        }
        if self.contains(.viewController) {
            filter.append(DebugDeallocFormat.viewController.tag())
        }
        if self.contains(.viewControllerCell) {
            filter.append(DebugDeallocFormat.viewControllerCell.tag())
        }
        if self.contains(.view) {
            filter.append(DebugDeallocFormat.view.tag())
        }
        if self.contains(.interactor) {
            filter.append(DebugDeallocFormat.interactor.tag())
        }
        if self.contains(.coordinator) {
            filter.append(DebugDeallocFormat.coordinator.tag())
        }
        if self.contains(.dataProvider) {
            filter.append(DebugDeallocFormat.dataProvider.tag())
        }

        return filter
    }
}
