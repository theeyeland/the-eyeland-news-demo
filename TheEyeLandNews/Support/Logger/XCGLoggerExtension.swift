//
//  XCGLoggerExtension.swift
//  RSFramework
//
//  Created by Zoltan Bognar on 07/12/16.
//  Copyright © 2020 The EyeLand s.r.o. All rights reserved.
//

import XCGLogger

extension XCGLogger: TheEyeLandExtensionsProvider {}

extension TheEyeLand where Base: XCGLogger {

    // Better dealloc debug information
    public func debugDealloc<T: BaseDebugDeallocProtocol>(withObject object: T) {
        base.debug("\(T.debugPreffix()) >>> \(type(of: object)) >>> is being deinitialized.", userInfo: [tags: T.debugTag()])
    }

    public func debugDealloc<T: BaseDebugDeallocProtocol>(withObject object: T, message: String) {
        base.debug("\(T.debugPreffix()) >>> \(type(of: object)) >>> is being deinitialized. > " + message, userInfo: [tags: T.debugTag()])
    }
}

extension TheEyeLand where Base: XCGLogger {

    public func deallocFilter(withOtions options: DebugDeallocOptions) {

        let filter = options.toStrings()
        base.filters = [TagFilter(includeFrom: filter)]
    }
}
