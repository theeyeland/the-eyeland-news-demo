//
//  Protocols.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 10.04.18.
//  Copyright © 2020 The EyeLand s.r.o. All rights reserved.
//

import Foundation

public protocol  BaseDebugDeallocProtocol {

    static func debugTag() -> String
    static func debugPreffix() -> String
}

public protocol  ViewModelDebugDeallocProtocol: BaseDebugDeallocProtocol {}

public protocol  AssemblyDebugDeallocProtocol: BaseDebugDeallocProtocol {}

public protocol  ViewControllerDebugDeallocProtocol: BaseDebugDeallocProtocol {}

public protocol  ViewControllerCellDebugDeallocProtocol: BaseDebugDeallocProtocol {}

public protocol  ViewDebugDeallocProtocol: BaseDebugDeallocProtocol {}

public protocol  InteractorDebugDeallocProtocol: BaseDebugDeallocProtocol {}

public protocol  CoordinatorDebugDeallocProtocol: BaseDebugDeallocProtocol {}

public protocol  DataProviderDebugDeallocProtocol: BaseDebugDeallocProtocol {}

extension BaseDebugDeallocProtocol {

    static func debugTag() -> String {
        return DebugDeallocFormat.common.tag()
    }

    static func debugPreffix() -> String {
        return DebugDeallocFormat.common.preffix()
    }
}

extension AssemblyDebugDeallocProtocol {

    public static func debugTag() -> String {
        return DebugDeallocFormat.assembly.tag()
    }

    public static func debugPreffix() -> String {
        return DebugDeallocFormat.assembly.preffix()
    }
}

extension ViewModelDebugDeallocProtocol {

    static func debugTag() -> String {
        return DebugDeallocFormat.viewModel.tag()
    }

    static func debugPreffix() -> String {
        return DebugDeallocFormat.viewModel.preffix()
    }
}

extension ViewControllerDebugDeallocProtocol {

    static func debugTag() -> String {
        return DebugDeallocFormat.viewController.tag()
    }

    static func debugPreffix() -> String {
        return DebugDeallocFormat.viewController.preffix()
    }
}

extension ViewControllerCellDebugDeallocProtocol {

    static func debugTag() -> String {
        return DebugDeallocFormat.viewControllerCell.tag()
    }

    static func debugPreffix() -> String {
        return DebugDeallocFormat.viewControllerCell.preffix()
    }
}

extension ViewDebugDeallocProtocol {

    static func debugTag() -> String {
        return DebugDeallocFormat.view.tag()
    }

    static func debugPreffix() -> String {
        return DebugDeallocFormat.view.preffix()
    }
}

extension InteractorDebugDeallocProtocol {

    static func debugTag() -> String {
        return DebugDeallocFormat.interactor.tag()
    }

    static func debugPreffix() -> String {
        return DebugDeallocFormat.interactor.preffix()
    }
}

extension CoordinatorDebugDeallocProtocol {

    static func debugTag() -> String {
        return DebugDeallocFormat.coordinator.tag()
    }

    static func debugPreffix() -> String {
        return DebugDeallocFormat.coordinator.preffix()
    }
}

extension DataProviderDebugDeallocProtocol {

    static func debugTag() -> String {
        return DebugDeallocFormat.dataProvider.tag()
    }

    static func debugPreffix() -> String {
        return DebugDeallocFormat.dataProvider.preffix()
    }
}
