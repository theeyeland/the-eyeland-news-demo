//
//  Logger.swift
//  RSFramework
//
//  Created by Zoltan Bognar on 05.12.16.
//  Copyright © 2020 The EyeLand s.r.o. All rights reserved.
//

import XCGLogger

public let tags = XCGLogger.Constants.userInfoKeyTags

public let log: XCGLogger = {

    let log = XCGLogger(identifier: "TheEyeLand logger")

    let dateFormatter = DateFormatter()
    dateFormatter.dateFormat = "dd-MM-yyyy hh:mm"
    dateFormatter.locale = Locale.current

    log.dateFormatter = dateFormatter

    log.setup(level: .debug,
              showLogIdentifier: false,
              showFunctionName: false ,
              showThreadName: false,
              showLevel: false,
              showFileNames: false,
              showLineNumbers: false,
              showDate: false,
              writeToFile: nil,
              fileLevel: nil)

    return log
}()
