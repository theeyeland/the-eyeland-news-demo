//
//  AppDelegateAssembly.swift
//  SlovakLines
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 08.06.18.
//  Copyright © 2018 freevision s.r.o. All rights reserved.
//

import Foundation
import Swinject

class AppDelegateAssebly: BaseAssembly {

    public override func assemble(container: Container) {
        container.register(AppDelegate.self) { _ in
            guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
                fatalError("Failed to get \(~|AppDelegate.self)")
            }
            return appDelegate

            }.inObjectScope(.container)
    }
}
