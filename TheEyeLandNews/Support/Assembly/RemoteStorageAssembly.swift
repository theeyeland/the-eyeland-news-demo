//
//  RemoteStorageAssembly.swift
//  TheEyeLandNews
//
//  Created by Zoltan Bognar The EyeLand on 08/01/2021.
//  Copyright © 2021 Mgr. Art. Ing. Zoltan Bognar. All rights reserved.
//

import Foundation
import Swinject

class RemoteStorageAssembly: BaseAssembly {

    public override func assemble(container: Container) {
        container.register(RemoteStorageProtocol.self) { resolver in
            return RemoteStorage()
        }.inObjectScope(.container)
    }
}
