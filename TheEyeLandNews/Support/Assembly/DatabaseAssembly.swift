//
//  DBSyncAssembly.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 10.04.18.
//  Copyright © 2020 The EyeLand s.r.o. All rights reserved.
//

import UIKit
import Swinject
import SwinjectStoryboard
import Sync

class DatabaseAssembly: BaseAssembly {

    public override func assemble(container: Container) {

        container.register(DataStack.self) { _ in
            DataStack(modelName: self.modelName)
            }.inObjectScope(.container)

        container.register(DatabaseProtocol.self) { resolver in
            let database = Database()
            database.dataStack = resolver.resolve(DataStack.self)!
            return database
        }.inObjectScope(.container)
    }

    let modelName: String

    init(withModelName modelName: String) {
        self.modelName = modelName
    }
}
