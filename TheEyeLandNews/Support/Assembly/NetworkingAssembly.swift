//
//  NetworkingAssembly.swift
//  TheEyeLandNews
//
//  Created by Zoltan Bognar The EyeLand on 03/12/2020.
//  Copyright © 2020 Mgr. Art. Ing. Zoltan Bognar. All rights reserved.
//

import Foundation
import Swinject
import Alamofire

class NetworkingAssembly: BaseAssembly {

    public override func assemble(container: Container) {

        container.register(Session.self) { _ in
            return Session(startRequestsImmediately: false)
            }.inObjectScope(.container)

        container.register(NetworkingProtocol.self) { resolver in
            let networking = Networking()
            networking.afSession = resolver.resolve(Session.self)!
            return networking
        }.inObjectScope(.transient)
    }
}
