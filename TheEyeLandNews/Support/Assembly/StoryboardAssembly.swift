//
//  BaseAssembly.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 29.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit
import Swinject
import SwinjectStoryboard
import Sync
import DATASource

class StoryboardAssembly: BaseAssembly {

    public override func assemble(container: Container) {
        container.register(SwinjectStoryboard.self, name: self.name) { _ in
            return SwinjectStoryboard.create(name: self.name, bundle: nil, container: container)
            }.inObjectScope(.container)
    }

    let name: String

    init(withName name: String) {
        self.name = name
    }

}
