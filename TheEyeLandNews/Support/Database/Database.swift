//
//  Database.swift
//  Slovak Lines
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 21.08.18.
//  Copyright © 2020 The EyeLand s.r.o. All rights reserved.
//

import Foundation
import CoreData
import Sync
import ReactiveSwift

struct DatabaseOperationOption: OptionSet {

    let rawValue: Int

    static let insert = DatabaseOperationOption(rawValue: 1 << 0)
    static let update = DatabaseOperationOption(rawValue: 1 << 1)
    static let delete = DatabaseOperationOption(rawValue: 1 << 2)
    static let insertRelationships = DatabaseOperationOption(rawValue: 1 << 3)
    static let updateRelationships = DatabaseOperationOption(rawValue: 1 << 4)
    static let deleteRelationships = DatabaseOperationOption(rawValue: 1 << 5)

    static let all: DatabaseOperationOption    = [.insert, .update, .delete, .insertRelationships, .updateRelationships, .deleteRelationships]
}

protocol DatabaseProtocol {

    func sync(_ changes: [[String: Any]], inEntityNamed entityName: String, operations: DatabaseOperationOption, apiPath: String) -> SignalProducer<Void, RemoteDataProviderError>
    func sync(_ changes: [[String: Any]], inEntityNamed entityName: String, operations: DatabaseOperationOption, completion: ((_ error: NSError?) -> Void)?)
}

class Database: DatabaseProtocol {

    var dataStack: DataStack!

    private var queue: OperationQueue = OperationQueue()

    func sync(_ changes: [[String: Any]], inEntityNamed entityName: String, operations: DatabaseOperationOption, apiPath: String) -> SignalProducer<Void, RemoteDataProviderError> {
        return SignalProducer<Void, RemoteDataProviderError> { [unowned self] observer, _ in

            self.sync(changes, inEntityNamed: entityName, operations: operations) { error in
                // New objects have been inserted
                // Existing objects have been updated
                // And not found objects have been deleted
                if let error = error {
                    let dbError = RemoteDataProviderError.dbSyncFailed(description: error.localizedDescription, apiPath: apiPath)
                    observer.send(error: dbError)
                }
                log.debug("🆗 Tickets SYNC sendCompleted")
                observer.sendCompleted()
            }
        }
    }

    func sync(_ changes: [[String: Any]], inEntityNamed entityName: String, operations: DatabaseOperationOption, completion: ((_ error: NSError?) -> Void)?) {

        log.debug("SYNC Database adress: \(MemoryAddress(of: self))")

        var syncOptions: Sync.OperationOptions = []
        if operations.contains(.insert) {
            syncOptions.insert(.insert)
            log.debug("➕ INSERT added to SYNC options")
        }
        if operations.contains(.update) {
            syncOptions.insert(.update)
            log.debug("➕ UPDATE added to SYNC options")
        }
        if operations.contains(.delete) {
            syncOptions.insert(.delete)
            log.debug("➕ DELETE added to SYNC options")
        }
        if operations.contains(.insertRelationships) {
            syncOptions.insert(.insertRelationships)
            log.debug("➕ INSERT RELATIONSHIPS added to SYNC options")
        }
        if operations.contains(.deleteRelationships) {
            syncOptions.insert(.deleteRelationships)
            log.debug("➕ DELETE RELATIONSHIPS added to SYNC options")
        }
        if operations.contains(.updateRelationships) {
            syncOptions.insert(.updateRelationships)
            log.debug("➕ UPDATE RELATIONSHIPS added to SYNC options")
        }
        if syncOptions.isEmpty {
            syncOptions = .all
            log.debug("➕ ALL added to SYNC options")
        }

        // sync without possibility to cancel but with error response
        // we can use this version if some problem happens during database sync
        dataStack.sync(changes, inEntityNamed: entityName, operations: syncOptions, completion: completion)

        // sync with possibility to cancel but no error response
//        let sync = Sync(changes: changes, inEntityNamed: entityName, dataStack: dataStack, operations: syncOptions)
//        sync.completionBlock = {
//            if let completion = completion {
//                completion(NSError(domain: "net.theyeland", code: 0, userInfo: ["sync error": "Database sync failed on operation queue"]))
//                completion(sync.error)
//            }
//        }
//
//        queue.addOperations([sync], waitUntilFinished: false)
    }

    func cancelSync() {
        queue.cancelAllOperations()
    }

    func drop() {
        dataStack.drop()
    }
}
