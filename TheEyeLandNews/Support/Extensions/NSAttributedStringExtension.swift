//
//  NSAttributedStringExtension.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 29.04.18.
//  Copyright © 2020 The EyeLand s.r.o. All rights reserved.
//

import UIKit

extension NSAttributedString {

    class func formatedHtml(from text: String, withFont font: UIFont, andColor color: UIColor) -> NSAttributedString? {

        let rgba = color.rgba

        let format = """
        <html style='margin:0; padding:0;text-align: left;'><span style="color:rgba(\(rgba.red),\(rgba.green),\(rgba.blue),\(rgba.alpha)); font-family:'\(font.familyName)'; font-size: \(font.pointSize)">%@</span></html>
        """

        let modifiedMessage = String(format: format, text)

        let htmlData = NSString(string: modifiedMessage).data(using: String.Encoding.unicode.rawValue)
        let options = [NSAttributedString.DocumentReadingOptionKey.documentType: NSAttributedString.DocumentType.html]
        let attributedString = try? NSMutableAttributedString(data: htmlData ?? Data(),
                                                              options: options,
                                                              documentAttributes: nil)

        return attributedString
    }
}

extension NSAttributedString {

    class func attributedUnderlined(with text: String,
                                    withFont font: UIFont,
                                    withColor color: UIColor,
                                    inFull fullText: String,
                                    withBaseFont baseFont: UIFont,
                                    andBaseColor baseColor: UIColor) -> NSAttributedString {

        let textString = NSMutableAttributedString(string: fullText, attributes: [
            NSAttributedString.Key.font: baseFont, NSAttributedString.Key.foregroundColor: baseColor
            ])

        let substringRange = textString.mutableString.range(of: text)

        if substringRange.location != NSNotFound {
            textString.addAttribute(NSAttributedString.Key.font, value: font, range: substringRange)
            textString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: substringRange)
            textString.addAttribute(NSAttributedString.Key.underlineStyle, value: 1, range: substringRange)
        }

        return textString
    }

    class func attributedMiddle(with text: String,
                                withFont font: UIFont,
                                withColor color: UIColor,
                                inFull fullText: String,
                                withBaseFont baseFont: UIFont,
                                andBaseColor baseColor: UIColor) -> NSAttributedString {

        let textString = NSMutableAttributedString(string: fullText, attributes: [
            NSAttributedString.Key.font: baseFont, NSAttributedString.Key.foregroundColor: baseColor
            ])

        let substringRange = textString.mutableString.range(of: text)

        if substringRange.location != NSNotFound {
            textString.addAttribute(NSAttributedString.Key.font, value: font, range: substringRange)
            textString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: substringRange)
        }

        return textString
    }

    class func attributed3Part(with text: String,
                               withFont font: UIFont,
                               withColor color: UIColor,
                               with secondText: String,
                               withSecondFont secondTextFont: UIFont,
                               withSecondColor seconeTextColor: UIColor,
                               inFull fullText: String,
                               withBaseFont baseFont: UIFont,
                               andBaseColor baseColor: UIColor) -> NSAttributedString {

        let textString = NSMutableAttributedString(string: fullText, attributes: [
            NSAttributedString.Key.font: baseFont, NSAttributedString.Key.foregroundColor: baseColor
        ])

        var substringRange = textString.mutableString.range(of: text)

        if substringRange.location != NSNotFound {
            textString.addAttribute(NSAttributedString.Key.font, value: font, range: substringRange)
            textString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: substringRange)
        }

        substringRange = textString.mutableString.range(of: secondText)

        if substringRange.location != NSNotFound {
            textString.addAttribute(NSAttributedString.Key.font, value: secondTextFont, range: substringRange)
            textString.addAttribute(NSAttributedString.Key.foregroundColor, value: seconeTextColor, range: substringRange)
        }

        return textString
    }

    class func attributedBegining(with text: String,
                                  withFont font: UIFont,
                                  withColor color: UIColor,
                                  inFull fullText: String,
                                  withBaseFont baseFont: UIFont,
                                  andBaseColor baseColor: UIColor) -> NSAttributedString {

        let textString = NSMutableAttributedString(string: fullText, attributes: [
            NSAttributedString.Key.font: baseFont, NSAttributedString.Key.foregroundColor: baseColor
            ])

        let substringRange = NSRange(location: 0, length: text.count )
        textString.addAttribute(NSAttributedString.Key.font, value: font, range: substringRange)
        textString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: substringRange)

        return textString
    }

    class func attributedBeginingAligned(with text: String,
                                         withFont font: UIFont,
                                         withColor color: UIColor,
                                         inFull fullText: String,
                                         withBaseFont baseFont: UIFont,
                                         andBaseColor baseColor: UIColor,
                                         withAlighment alighment: NSTextAlignment = .left) -> NSAttributedString {

        let paragraphStyle = NSMutableParagraphStyle()
        paragraphStyle.alignment = alighment

        let textString = NSMutableAttributedString(string: fullText, attributes: [
            NSAttributedString.Key.font: baseFont, NSAttributedString.Key.foregroundColor: baseColor,
            NSAttributedString.Key.paragraphStyle: paragraphStyle
            ])

        let substringRange = NSRange(location: 0, length: text.count )
        textString.addAttribute(NSAttributedString.Key.font, value: font, range: substringRange)
        textString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: substringRange)

        return textString
    }

    class func attributedTitleBegining(with text: String,
                                       withFont font: UIFont,
                                       withColor color: UIColor,
                                       inFull fullText: String,
                                       withBaseFont baseFont: UIFont,
                                       andBaseColor baseColor: UIColor) -> NSAttributedString {

        let textString = NSMutableAttributedString(string: fullText, attributes: [
            NSAttributedString.Key.font: baseFont, NSAttributedString.Key.foregroundColor: baseColor
            ])

        let paragraph = NSMutableParagraphStyle()
        paragraph.alignment = .center

        let substringRange = NSRange(location: 0, length: text.count )
        textString.addAttribute(NSAttributedString.Key.font, value: font, range: substringRange)
        textString.addAttribute(NSAttributedString.Key.foregroundColor, value: color, range: substringRange)
        textString.addAttribute(NSAttributedString.Key.paragraphStyle, value: paragraph, range: substringRange)

        return textString
    }
}
