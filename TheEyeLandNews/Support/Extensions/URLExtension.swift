//
//  URLExtension.swift
//  TheEyeLandNews
//
//  Created by Zoltan Bognar The EyeLand on 24/03/2021.
//  Copyright © 2021 Mgr. Art. Ing. Zoltan Bognar. All rights reserved.
//

import Foundation
import ReactiveSwift
import Swinject
import Combine

extension URL {

    static func storageURLProducer(relativePath: String?) -> SignalProducer<URL?, Never> {
        guard let relativePath = relativePath else { return SignalProducer(value: nil) }

        if TheEyeLandNews.Router.urlFormat == .remoteStorege {
            let storage = Assembler.sharedAssembler.resolver.resolve(RemoteStorageProtocol.self)!
            return storage.urlProducer(for: relativePath).flatMapError { (error) -> SignalProducer<URL?, Never> in
                return SignalProducer<URL?, Never>(value: nil)
            }
        } else {
            return SignalProducer(value: URL(string: TheEyeLandNews.Router.baseWebSubfolderURLPath + relativePath))
        }
    }

    static func storageURLProducerCF(relativePath: String?) -> AnyPublisher<URL?, Never> {
        guard let relativePath = relativePath else { return Just<URL?>(nil).eraseToAnyPublisher() }

        if TheEyeLandNews.Router.urlFormat == .remoteStorege {
            let storage = Assembler.sharedAssembler.resolver.resolve(RemoteStorageProtocol.self)!
            return storage.urlProducerCF(for: relativePath).replaceError(with: nil).flatMap { (url) -> AnyPublisher<URL?, Never> in
                return Just<URL?>(url).eraseToAnyPublisher()
            }.eraseToAnyPublisher()
        } else {
            return Just<URL?>(URL(string: TheEyeLandNews.Router.baseWebSubfolderURLPath + relativePath)).eraseToAnyPublisher()
        }
    }
}
