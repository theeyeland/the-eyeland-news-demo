//
//  RemoteStorage.swift
//  TheEyeLandNews
//
//  Created by Zoltan Bognar The EyeLand on 08/01/2021.
//  Copyright © 2021 Mgr. Art. Ing. Zoltan Bognar. All rights reserved.
//

import Foundation
import FirebaseStorage
import ReactiveSwift
import Combine

protocol RemoteStorageProtocol {

    var reference: StorageReference {get}
    func urlProducer(for path: String) -> SignalProducer<URL?, RemoteDataProviderError>
    func urlProducerCF(for path: String) -> AnyPublisher<URL?, RemoteDataProviderError>
}

class RemoteStorage: RemoteStorageProtocol {

    private let cache = NSCache<NSString, NSURL>()

    lazy var reference: StorageReference = {
        return Storage.storage().reference()
    }()

    func urlProducer(for path: String) -> SignalProducer<URL?, RemoteDataProviderError> {
        return SignalProducer<URL?, RemoteDataProviderError> { [unowned self] observer, _ in

            if let url = self.cache.object(forKey: path as NSString) as URL? {
                log.debug("urlProducer() from cashe \(String(describing: url))")
                observer.send(value: url)
                observer.sendCompleted()
            }

            self.reference.child(path).downloadURL { [unowned self] url, error in
                if let error = error {
                    // Handle any errors
                    log.debug(error)
                    observer.send(error: RemoteDataProviderError.dataRequestFailed(description: error.localizedDescription,
                                                                                   statusCode: 0,
                                                                                   data: Data(),
                                                                                   apiPath: path))
                } else {
                    log.debug("urlProducer() \(String(describing: url))")
                    if let url = url {
                        self.cache.setObject(url as NSURL, forKey: path as NSString)
                    }
                    observer.send(value: url)
                    observer.sendCompleted()
                }
            }
        }
    }

    func urlProducerCF(for path: String) -> AnyPublisher<URL?, RemoteDataProviderError> {
        return Future<URL?, RemoteDataProviderError> { [unowned self] promises in

            if let url = self.cache.object(forKey: path as NSString) as URL? {
                return promises(.success(url))
            }

            self.reference.child(path).downloadURL { [unowned self] url, error in
                if let error = error {
                    // Handle any errors
                    log.debug(error)
                    let error = RemoteDataProviderError.dataRequestFailed(description: error.localizedDescription,
                                                                                   statusCode: 0,
                                                                                   data: Data(),
                                                                                   apiPath: path)
                    promises(.failure(error))
                } else {
                    if let url = url {
                        log.debug("urlProducerCF() \(url)")
                        self.cache.setObject(url as NSURL, forKey: path as NSString)
                        promises(.success(url))
                    } else {
                        let error = RemoteDataProviderError.dataRequestFailed(description: RemoteDataProviderError.createRequestFaildDescription,
                                                                                       statusCode: 0,
                                                                                       data: Data(),
                                                                                       apiPath: path)
                        promises(.failure(error))
                    }
                }
            }
        }.eraseToAnyPublisher()
    }
}
