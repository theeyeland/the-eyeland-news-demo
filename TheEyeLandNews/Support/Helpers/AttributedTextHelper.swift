//
//  AttributedTextHelper.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 15.05.18.
//  Copyright © 2020 The EyeLand s.r.o. All rights reserved.
//

import UIKit

class AttributedTextHelper {

    class func attributedText(_ firstText: String, _ secondText: String, _ separator: String = "\n" ) -> NSAttributedString {

        guard let skin = SkinStore.sharedInstance.skin else { return NSAttributedString() }

        return NSAttributedString.attributedBegining(with: firstText,
                                                     withFont: skin.normalTextBoldFont,
                                                     withColor: skin.normalTextColor,
                                                     inFull: firstText + separator + secondText,
                                                     withBaseFont: skin.normalSmallTextFont,
                                                     andBaseColor: skin.normalTextLightColor)
    }
}
