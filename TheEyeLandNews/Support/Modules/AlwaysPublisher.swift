//
//  AlwaysPublisher.swift
//  TheEyeLandNews
//
//  Created by Zoltan Bognar The EyeLand on 05/03/2021.
//  Copyright © 2021 Mgr. Art. Ing. Zoltan Bognar. All rights reserved.
//

import Foundation
import Combine

/// `Always` is a publisher that continuously emits the
/// same output as long as there is demand.
public struct Always<Output>: Publisher {

  public typealias Failure = Never

  public let output: Output

  public init(_ output: Output) {
    self.output = output
  }

  public func receive<S: Subscriber>(subscriber: S) where S.Input == Output, S.Failure == Failure {
    let subscription = Subscription(output: output, subscriber: subscriber)
    subscriber.receive(subscription: subscription)
  }
}

private extension Always {

  final class Subscription<S: Subscriber> where S.Input == Output, S.Failure == Failure {
    private let output: Output
    private var subscriber: S?

    init(output: Output, subscriber: S) {
      self.output = output
      self.subscriber = subscriber
    }
  }
}

extension Always.Subscription: Subscription {

  func request(_ demand: Subscribers.Demand) {
    // TODO: Respond to demand by emitting values

    /// infinite demand - not correct version
//    guard let subscriber = subscriber else { return }
//    var demand = demand
//    while demand > 0 {
//      demand -= 1
//      let newDemand = subscriber.receive(output)
//      demand += newDemand
//    }

    /// Correct version
    var demand = demand
    while let subscriber = subscriber, demand > 0 {
        demand -= 1
        demand += subscriber.receive(output)
    }
  }
}

extension Always.Subscription: Cancellable {

  func cancel() {
    subscriber = nil
  }
}
