//
//  AnyRemoteDataProvider.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 10.04.18.
//  Copyright © 2020 The EyeLand s.r.o. All rights reserved.
//

import ReactiveSwift
import Combine

public enum RemoteDataProviderError: Error, Equatable {

    case none
    case dataRequestFailed(description: String, statusCode: Int, data: Data, apiPath: String)
    case dataDownloadFailed(description: String, statusCode: Int, data: Data, apiPath: String)
    case dataMappingFailed(apiPath: String)
    case dbSyncFailed(description: String, apiPath: String)
    case noNetworkConnection

    static let createRequestFaildDescription = " Create URLRequest failed, Error = nil"
    static let defaultDescription = "No Descritpion, Error = nil"
    static let jsonCastFailedDescription = "FAILED to cast JSON as? [String: Any]"
    static let failedToGetJSONDescription = "FAILD get JSON from response.result.value"
}

extension RemoteDataProviderError {

    public static func == (lhs: RemoteDataProviderError, rhs: RemoteDataProviderError) -> Bool {
        switch (lhs, rhs) {
        case (.none, .none): return true
        case (.dataMappingFailed, .dataMappingFailed): return true
        case (.dataRequestFailed, .dataRequestFailed): return true
        default: return false
        }
    }
}

enum StatusCodesFormat: Int {
    case none = 0
    case sucssess = 200
    case noContent = 204 // it is used for delete user in backend
    case badRequest = 400
    case forbidden = 403
    case notFound = 404
    case serverError = 500
    case noInternetConnection = 9999
    case cardWasRefused = 10000
    case cardProcessingFailed = 10001
    case graphQLResponseFailed = 10002
    case apolloResponseFailed = 10003
    case accessTokenInvalid = 10004

    static let defaultStatusCode = 500
}

// RemoteDataCodableProviderProtocol Type Eraser
protocol RemoteDataCodableProviderProtocol: BaseDataProviderProtocol {

    associatedtype Input: NetworkingRouterProtocol
    associatedtype Output: Decodable

    var update: Action<Input, Void, RemoteDataProviderError> {get}
    var fetch: Action<Input, [Output], RemoteDataProviderError> {get}
    var fetchObject: Action<Input, Output, RemoteDataProviderError> {get}
    var fetchCF: Combine.Action<Input, [Output], RemoteDataProviderError> {get}
}

private class _AnyRemoteDataCodableProviderProtocolBase<Input: NetworkingRouterProtocol, Output: Decodable>: RemoteDataCodableProviderProtocol {

    init() {
        guard type(of: self) != _AnyRemoteDataCodableProviderProtocolBase.self else {
            fatalError("_AnyRemoteDataProviderProtocolBase<Output,Input> instances can not be created; create a subclass instance instead")
        }
    }

    var update: Action<Input, Void, RemoteDataProviderError> {
        fatalError("Must override")
    }

    var fetch: Action<Input, [Output], RemoteDataProviderError> {
        fatalError("Must override")
    }

    var fetchObject: Action<Input, Output, RemoteDataProviderError> {
        fatalError("Must override")
    }

    var fetchCF: Combine.Action<Input, [Output], RemoteDataProviderError> {
        fatalError("Must override")
    }

}

private final class _AnyRemoteDataCodableProviderProtocolBox<Concrete: RemoteDataCodableProviderProtocol>: _AnyRemoteDataCodableProviderProtocolBase<Concrete.Input, Concrete.Output> {

    var concrete: Concrete

    init(_ concrete: Concrete) {
        self.concrete = concrete
    }

    override var update: Action<Concrete.Input, Void, RemoteDataProviderError> {
        return concrete.update
    }

    override var fetch: Action<Concrete.Input, [Concrete.Output], RemoteDataProviderError> {
        return concrete.fetch
    }

    override var fetchObject: Action<Concrete.Input, Concrete.Output, RemoteDataProviderError> {
        return concrete.fetchObject
    }

    override var fetchCF: Combine.Action<Concrete.Input, [Concrete.Output], RemoteDataProviderError> {
        return concrete.fetchCF
    }
}

final class AnyRemoteDataCodableProvider<Input: NetworkingRouterProtocol, Output: Decodable>: RemoteDataCodableProviderProtocol {

    private let box: _AnyRemoteDataCodableProviderProtocolBase<Input, Output>

    init<Concrete: RemoteDataCodableProviderProtocol>(_ concrete: Concrete) where Concrete.Input == Input, Concrete.Output == Output {
        box = _AnyRemoteDataCodableProviderProtocolBox(concrete)
    }

    var update: Action<Input, Void, RemoteDataProviderError> {
        return box.update
    }

    var fetch: Action<Input, [Output], RemoteDataProviderError> {
        return box.fetch
    }

    var fetchObject: Action<Input, Output, RemoteDataProviderError> {
        return box.fetchObject
    }

    var fetchCF: Combine.Action<Input, [Output], RemoteDataProviderError> {
        return box.fetchCF
    }
}
