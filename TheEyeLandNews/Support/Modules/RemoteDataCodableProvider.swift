//
//  RemoteDataCodableProvider.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 10.04.18.
//  Copyright © 2020 The EyeLand s.r.o. All rights reserved.
//

import Foundation
import ReactiveSwift
import Combine

class RemoteDataCodableProvider<Input: NetworkingRouterProtocol, Output: Decodable>: BaseDataProvider, RemoteDataCodableProviderProtocol {

    var networking: NetworkingProtocol!

    lazy var update: Action<Input, Void, RemoteDataProviderError>  = {
        return Action<Input, Void, RemoteDataProviderError> { [unowned self] router in
            return self.isConnection.then(self.networking.update(withRouter: router))
        }
    }()

    lazy var fetch: Action<Input, [Output], RemoteDataProviderError>  = {
        return Action<Input, [Output], RemoteDataProviderError> { [unowned self] router in
            return self.isConnection.then(self.networking.fetchArray(withRouter: router))
        }
    }()

    lazy var fetchObject: Action<Input, Output, RemoteDataProviderError>  = {
        return Action<Input, Output, RemoteDataProviderError> { [unowned self] router in
            return self.isConnection.then(self.networking.fetchObject(withRouter: router))
        }
    }()

    lazy var fetchCF: Combine.Action<Input, [Output], RemoteDataProviderError>  = {
        return Combine.Action<Input, [Output], RemoteDataProviderError> { [unowned self] router in
            return self.isConnectionCF.flatMap({ (value: Bool) -> AnyPublisher<[Output], RemoteDataProviderError> in
                return self.networking.fetchArrayCF(withRouter: router)
            }).eraseToAnyPublisher()
        }
    }()
}
