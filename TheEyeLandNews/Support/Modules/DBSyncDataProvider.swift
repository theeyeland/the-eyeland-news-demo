//
//  DBSyncDataProvider.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 20.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit

import ReactiveSwift

class DBSyncDataProvider<Input: NetworkingRouterProtocol>: BaseDataProvider {

    var networking: NetworkingProtocol!
    var database: DatabaseProtocol!
    var entityName: String!
    var operations: DatabaseOperationOption = [.all]

    private func syncToDB(fromJSONArray jsonArray: [[String: Any]]) -> SignalProducer<Void, RemoteDataProviderError> {
        return database.sync(jsonArray, inEntityNamed: entityName, operations: operations, apiPath: "")
    }

    lazy var fetch: Action<Input, Void, RemoteDataProviderError>  = {
        return Action<Input, Void, RemoteDataProviderError> { [unowned self] router in
            return self.isConnection.then(self.networking.fetchJSON(withRouter: router)).take(during: self.reactive.lifetime)
                        .flatMap(.latest) { [unowned self] jsonArray -> SignalProducer<Void, RemoteDataProviderError> in
                            return self.syncToDB(fromJSONArray: jsonArray) // .take(during: self.reactive.lifetime)
                    }
            }
        }()
}
