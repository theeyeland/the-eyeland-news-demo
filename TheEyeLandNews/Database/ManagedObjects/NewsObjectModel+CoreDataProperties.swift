//
//  NewsObjectModel+CoreDataProperties.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 20.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import Foundation
import CoreData

extension NewsObjectModel {

    @nonobjc public class func createFetchRequest() -> NSFetchRequest<NewsObjectModel> {
        return NSFetchRequest<NewsObjectModel>(entityName: "NewsObjectModel")
    }

    @NSManaged public var appstoreId: Int32
    @NSManaged public var imageLink: String?
    @NSManaged public var newsId: Int32
    @NSManaged public var title: String?
    @NSManaged public var subtitle: String?
    @NSManaged public var webLink: String?
    @NSManaged public var obsolete: Bool
    @NSManaged public var application: ApplicationObjectModel?

}
