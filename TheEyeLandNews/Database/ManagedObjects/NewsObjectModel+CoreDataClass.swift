//
//  NewsObjectModel+CoreDataClass.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 14.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import Foundation
import CoreData

@objc(NewsObjectModel)
public class NewsObjectModel: NSManagedObject {

}
