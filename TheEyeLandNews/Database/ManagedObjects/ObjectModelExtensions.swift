//
//  ObjectModelExtensions.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 22.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import Foundation
import ReactiveSwift
import Swinject

extension NewsObjectModel: AppURLCoordintatorProtocol {

    func appStoreURL() throws -> URL {
        return try "\(TheEyeLandNews.Constants.appStoreLink)\(appstoreId)".asURL()
    }

    func webURL() -> URL? {
        if let webLink = webLink {
            return URL(string: TheEyeLandNews.Constants.baseWebURLPath + webLink)
        }

        return nil
    }
}
