//
//  ApplicationObjectModel+CoreDataProperties.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 20.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import Foundation
import CoreData

extension ApplicationObjectModel {

    @nonobjc public class func createFetchRequest() -> NSFetchRequest<ApplicationObjectModel> {
        return NSFetchRequest<ApplicationObjectModel>(entityName: "ApplicationObjectModel")
    }

    @NSManaged public var applicationId: Int32
    @NSManaged public var imageLink: String?
    @NSManaged public var title: String?
    @NSManaged public var subtitle: String?
    @NSManaged public var news: NSSet?

}

// MARK: Generated accessors for news
extension ApplicationObjectModel {

    @objc(addNewsObject:)
    @NSManaged public func addToNews(_ value: NewsObjectModel)

    @objc(removeNewsObject:)
    @NSManaged public func removeFromNews(_ value: NewsObjectModel)

    @objc(addNews:)
    @NSManaged public func addToNews(_ values: NSSet)

    @objc(removeNews:)
    @NSManaged public func removeFromNews(_ values: NSSet)

}
