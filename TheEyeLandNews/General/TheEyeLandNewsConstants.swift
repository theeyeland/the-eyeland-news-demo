//
//  Constants.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 22.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import Foundation

struct TheEyeLandNews { }

extension TheEyeLandNews {

    struct Constants {

        static let appStoreLink = "itms-apps://itunes.apple.com/app/id"
        static let baseWebURLPath = "https://www.theeyeland.net"

        static let baseWebSubfolderURLPath = Constants.baseWebURLPath

        struct Logger {

            static let mapping = "mapping"
        }

        struct Storyboard {

            static let main = "Main"
            static let objMapCombineApps = "ObjMapCombineApps"
            static let objMapSwiftUIView = "ObjMapSwiftUIView"
        }

        struct Database {

            static let modelName = "TheEyeLandNews"

        }

        struct Common {

            static var yes: String { return "Yes" }
            static var no: String { return "No" }
            static var ok: String { return "Ok" }
            static var done: String { return "Done" }
            static var cancel: String { return "Cancel" }
            static var edit: String { return "Edit" }
            static var delete: String { return "Delete" }
            static var confirm: String { return "Confirm" }
        }

        struct Text {

            static let objectMappingAppsVCTitle = "ObjMap Apps"
            static let objectMappingAppNewsVCTitle = "ObjMap App News"
            static let dbSyncAppsVCTitle = "DBSync Apps"
            static let dbSyncAppNewsVCTitle = "DBSync App News "
            static let objMapCombineVCTitle = "Combine Apps"
            static let objMapCombineAppNewsVCTitle = "Combine App News"
            static let swiftUICombineVCTitle = "SwiftUI Apps"
            static let swiftUICombineAppNewsVCTitle = "SwiftUI App News"

            static let noInternetConncetionTitle = "No Internet Connection"
            static let noInternetConncetionMessage = "Please try again later."
        }
    }
}
