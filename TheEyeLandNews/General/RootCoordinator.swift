//
//  RootCoordinator.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 04.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit
import Swinject
import SwinjectStoryboard

protocol RootCoordinatorProtocol: BaseCoordinatorProtocol {

    func present() -> UITabBarController
}

final class RootCoordinator: BaseCoordinator, RootCoordinatorProtocol {

    func present() -> UITabBarController {

        var sb = assembler.resolver.resolve(SwinjectStoryboard.self, name: TheEyeLandNews.Constants.Storyboard.main)!

        guard let objectMappingViewController
            = sb.instantiateViewController(withIdentifier: ~|ObjectMappingAppsViewController.self) as? ObjectMappingAppsViewController else {
                fatalError("Failed to create \(~|ObjectMappingAppsViewController.self)")
        }

        guard let dbSyncController
            = sb.instantiateViewController(withIdentifier: ~|DBSyncAppsViewController.self) as? DBSyncAppsViewController else {
                fatalError("Failed to create \(~|DBSyncAppsViewController.self)")
        }

        sb = assembler.resolver.resolve(SwinjectStoryboard.self, name: TheEyeLandNews.Constants.Storyboard.objMapCombineApps)!
        guard let objMapCombineAppsViewController
            = sb.instantiateViewController(withIdentifier: ~|ObjMapCombineAppsViewController.self) as? ObjMapCombineAppsViewController else {
                fatalError("Failed to create \(~|ObjMapCombineAppsViewController.self)")
        }

        sb = assembler.resolver.resolve(SwinjectStoryboard.self, name: TheEyeLandNews.Constants.Storyboard.objMapSwiftUIView)!
        guard let objMapSwiftUIHostController
            = sb.instantiateViewController(withIdentifier: ~|SwiftUIViewHostingController.self) as? SwiftUIViewHostingController else {
                fatalError("Failed to create \(~|SwiftUIViewHostingController.self)")
        }

        let tabBarCtrl = UITabBarController()
        tabBarCtrl.viewControllers = [objectMappingViewController.navigationController!,
                                      dbSyncController.navigationController!,
                                      objMapCombineAppsViewController.navigationController!,
                                      objMapSwiftUIHostController]

        return tabBarCtrl
    }

    func presentObjectMappingOnly() -> UINavigationController {

        let sb = assembler.resolver.resolve(SwinjectStoryboard.self, name: TheEyeLandNews.Constants.Storyboard.main)!

        guard let objectMappingViewController
            = sb.instantiateViewController(withIdentifier: ~|ObjectMappingAppsViewController.self) as? ObjectMappingAppsViewController else {
                fatalError("Failed to create \(~|ObjectMappingAppsViewController.self)")
        }

        return objectMappingViewController.navigationController!
    }
}
