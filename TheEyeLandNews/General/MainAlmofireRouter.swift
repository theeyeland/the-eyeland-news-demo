//
//  MainAlmofireRouter.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 01.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit

extension TheEyeLandNews {

    public enum Router: String, NetworkingRouterProtocol {

        static let baseWebSubfolderURLPath = TheEyeLandNews.Constants.baseWebSubfolderURLPath

        case dbSyncApps
        case objectMappingApps
        case starScalesPro
        case starScalesLite
        case starScalesHD
        case vCreatorMLM
        case doMindEN
        case doMindDE
        case doMindES
        case doMindIT
        case doMindGR
        case doMindSK
        case doMindFR
        case doMindHU

        static let applicationRouters: [Router] = [.starScalesPro,
                                                  .starScalesHD,
                                                  .starScalesLite,
                                                  .vCreatorMLM,
                                                  .doMindEN,
                                                  .doMindDE,
                                                  .doMindES,
                                                  .doMindIT,
                                                  .doMindGR,
                                                  .doMindSK,
                                                  .doMindFR,
                                                  .doMindHU]

        var authentification: NetworkingAuthentification {
            return .none
        }

        static let urlFormat: URLFormat = .remoteStorege

        var urlFormat: URLFormat {
            return Router.urlFormat
        }

        var method: String {
            switch self {
            case .dbSyncApps, .objectMappingApps:
                return "GET"
            case .starScalesPro, .starScalesLite, .starScalesHD:
                return "GET"
            case .vCreatorMLM:
                return "GET"
            case .doMindEN, .doMindDE, .doMindES, .doMindIT, .doMindGR, .doMindSK, .doMindFR, .doMindHU:
                return "GET"
            }
        }

        func appendBasePath(toRelative path: String) -> String {
//            if Router.urlFormat == .remoteStorege {
                return path
//            } else {
//                return TheEyeLandNews.Router.baseWebSubfolderURLPath + path
//            }
        }

        var path: String {
            switch self {
            case .dbSyncApps:
                return appendBasePath(toRelative: "/dbsync-apps.json")
            case .objectMappingApps:
                return appendBasePath(toRelative: "/objectmapping-apps.json")
            case .starScalesPro:
                return appendBasePath(toRelative: "/starscalespro.json")
            case .starScalesLite:
                return appendBasePath(toRelative: "/starscaleslite.json")
            case .starScalesHD:
                return appendBasePath(toRelative: "/starscaleshd.json")
            case .vCreatorMLM:
                return appendBasePath(toRelative: "/vcreatormlm.json")
            case .doMindEN:
                return appendBasePath(toRelative: "/dominden.json")
            case .doMindDE:
                return appendBasePath(toRelative: "/domindde.json")
            case .doMindES:
                return appendBasePath(toRelative: "/domindes.json")
            case .doMindIT:
                return appendBasePath(toRelative: "/domindit.json")
            case .doMindGR:
                return appendBasePath(toRelative: "/domindgr.json")
            case .doMindSK:
                return appendBasePath(toRelative: "/domindsk.json")
            case .doMindFR:
                return appendBasePath(toRelative: "/domindfr.json")
            case .doMindHU:
                return appendBasePath(toRelative: "/domindhu.json")
            }
        }

//        public func asURL() throws -> URL {
//
//        }

        var keypath: String {
            switch self {
            case .dbSyncApps, .objectMappingApps:
                return "applications"
            case .starScalesPro, .starScalesHD, .starScalesLite:
                return "news"
            case .vCreatorMLM:
                return "news"
            case .doMindEN, .doMindDE, .doMindES, .doMindIT, .doMindGR, .doMindSK, .doMindFR, .doMindHU:
                return "news"
            }
        }

        var jsonEncoding: Bool {
            switch self {
            default:
                return false
            }
        }

        var languageCode: String {
            switch self {
            default:
                if let languageCode = Locale.current.languageCode {
                    return "\(languageCode)-EN"
                }
                return "en-EN"
            }
        }

        var parameters: [String: Any] {
            switch self {
            default:
                return [:]
            }
        }
    }
}
