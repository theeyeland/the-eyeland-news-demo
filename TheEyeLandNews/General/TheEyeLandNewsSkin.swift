//
//  CustomSkin.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 11.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit

struct Palette {

    let c1Color = UIColor(hexString: "#EE2E33")
    let c2Color = UIColor(hexString: "#1A245F")
    let c2bColor = UIColor(hexString: "#1A245F", alpha: 0.6)
    let c3Color = UIColor(hexString: "#009BBF")
    let c3bColor = UIColor(hexString: "#009BBF", alpha: 0.6)
    let c4Color = UIColor(hexString: "#EA6852")
    let c4bColor = UIColor(hexString: "#EA6852", alpha: 0.6)
    let c5Color = UIColor(hexString: "#FFFFFF")
    let c5bColor = UIColor(hexString: "#FFFFFF", alpha: 0.6)
    let c6Color = UIColor(hexString: "#FFFFFF")
    let c6bColor = UIColor(hexString: "#FFFFFF", alpha: 0.6)
    let c7Color = UIColor(hexString: "#333333")
    let c8Color = UIColor(hexString: "#666666")
    let c9Color = UIColor(hexString: "#999999")
    let c10Color = UIColor(hexString: "#CCCCCC")
    let c11Color = UIColor(hexString: "#FFFFFF")
    let c12Color = UIColor(hexString: "#F6F6F6")
    let c13Color = UIColor(hexString: "#EBEBEB")
    let c14Color = UIColor(hexString: "#FFFFFF")
    let c15Color = UIColor(hexString: "#FFFFFF")
    let c15bColor = UIColor(hexString: "#FFFFFF", alpha: 0.5)

    let h1Font: UIFont = UIDevice.isIPad() ? UIFont.fontOrDefault(name: "Avenir-Light", size: 35) : UIFont.fontOrDefault(name: "Avenir-Light", size: 24)
    let h2Font: UIFont = UIDevice.isIPad() ? UIFont.fontOrDefault(name: "Avenir-Light", size: 30) : UIFont.fontOrDefault(name: "Avenir-Light", size: 19)
    let t3rFont: UIFont = UIDevice.isIPad() ? UIFont.fontOrDefault(name: "Avenir-Light", size: 19) : UIFont.fontOrDefault(name: "Avenir-Light", size: 16)
    let t3bFont: UIFont = UIDevice.isIPad() ? UIFont.fontOrDefault(name: "Avenir-Heavy", size: 19) : UIFont.fontOrDefault(name: "Avenir-Heavy", size: 16)
    let t4rFont: UIFont = UIDevice.isIPad() ? UIFont.fontOrDefault(name: "Avenir-Light", size: 15) : UIFont.fontOrDefault(name: "Avenir-Light", size: 14)
    let t4bFont: UIFont = UIDevice.isIPad() ? UIFont.fontOrDefault(name: "Avenir-Heavy", size: 15) : UIFont.fontOrDefault(name: "Avenir-Heavy", size: 14)
    let t5rFont: UIFont = UIDevice.isIPad() ? UIFont.fontOrDefault(name: "Avenir-Light", size: 13) : UIFont.fontOrDefault(name: "Avenir-Light", size: 12)
    let t5bFont: UIFont = UIDevice.isIPad() ? UIFont.fontOrDefault(name: "Avenir-Heavy", size: 13) : UIFont.fontOrDefault(name: "Avenir-Heavy", size: 12)
    let bFont: UIFont = UIDevice.isIPad() ? UIFont.fontOrDefault(name: "Avenir-Heavy", size: 16) : UIFont.fontOrDefault(name: "Avenir-Heavy", size: 16)
    let fFont: UIFont = UIDevice.isIPad() ? UIFont.fontOrDefault(name: "Avenir-Heavy", size: 17) : UIFont.fontOrDefault(name: "Avenir-Heavy", size: 14)
    let grFont: UIFont = UIDevice.isIPad() ? UIFont.fontOrDefault(name: "MaterialIcons-Regular", size: 24) : UIFont.fontOrDefault(name: "MaterialIcons-Regular", size: 24)
    let gsFont: UIFont = UIDevice.isIPad() ? UIFont.fontOrDefault(name: "MaterialIcons-Regular", size: 19) : UIFont.fontOrDefault(name: "MaterialIcons-Regular", size: 19)
}

struct TheEyeLandNewsSkin: Skin {

    let palette = Palette()

    var mainColor: UIColor { return palette.c2Color }
    var titleFont: UIFont {get {return palette.h1Font }}
    var titleColor: UIColor {get {return palette.c10Color}}
    var subTitleFont: UIFont {get {return palette.h2Font }}
    var subTitleColor: UIColor {get {return palette.c10Color}}

    var normalTextFont: UIFont {get {return palette.t3rFont }}
    var normalTextBoldFont: UIFont {get {return palette.t3bFont }}
    var normalTextColor: UIColor {get {return palette.c7Color }}
    var normalTextLightColor: UIColor {get {return palette.c7Color.withAlphaComponent(0.7) }}
    var normalSmallTextFont: UIFont {get {return palette.t5rFont }}

    var mainBackgroundColor: UIColor {get {return palette.c11Color }}
    var navigationBackgroundColor: UIColor {get {return palette.c12Color }}

    var navigationBarFont: UIFont { return palette.t3bFont }
}
