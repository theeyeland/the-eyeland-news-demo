//
//  ObjectMappingAppNewsAssembly.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 29.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit
import Swinject
import SwinjectStoryboard

class ObjectMappingAppNewsAssembly: BaseAssembly {

    public override func assemble(container: Container) {

        // Coordinator
        container.register(ObjectMappingAppNewsCoordinator.self) { _ in ObjectMappingAppNewsCoordinator() }
            .initCompleted { (resolver, coordinator: ObjectMappingAppNewsCoordinator) in
                if let navigationConroller = self.navigationConroller {
                    coordinator.navigationController = navigationConroller
                }
        }

        // DataProvider
        container.register(ObjectMappingAppNewsDataProvider.self) { resolver in
            let rdp = RemoteDataCodableProvider<TheEyeLandNews.Router, ObjectMappingAppNewsModel>()
            rdp.networking = resolver.resolve(NetworkingProtocol.self)!
            return ObjectMappingAppNewsDataProvider(rdp)
        }

        // Ineractor
        container.register(ObjectMappingAppNewsInteractorProtocol.self) { resolver in
            let interactor = ObjectMappingAppNewsInteractor(dataProvider: resolver.resolve(ObjectMappingAppNewsDataProvider.self)!)
            interactor.router = self.router
            return interactor
        }.initCompleted { (_, interactor) in
            interactor.initialize()
        }

        // ViewModel
        container.register(ObjectMappingAppNewsViewModel.self) { resolver in
            ObjectMappingAppNewsViewModel(interactor: resolver.resolve(ObjectMappingAppNewsInteractorProtocol.self)!,
                                       coordinator: resolver.resolve(ObjectMappingAppNewsCoordinator.self)!)
        }.initCompleted { (_, viewModel) in
            viewModel.initialize()
        }

        // Controller
        container.storyboardInitCompleted(ObjectMappingAppNewsCollectionViewController.self) { resolver, contorller in
            contorller.viewModel = resolver.resolve(ObjectMappingAppNewsViewModel.self)!
        }
    }

    private weak var navigationConroller: UINavigationController?
    private var router: TheEyeLandNews.Router = .starScalesPro

    public init(_ navigationConroller: UINavigationController?, _ router: String) {
        self.navigationConroller = navigationConroller

        if let mainRouter = TheEyeLandNews.Router(rawValue: router) {
            self.router = mainRouter
        } else {
            log.error("TheEyeLandNews.Router can't be initialized from ObjectMappingAppsModel.router = \(router)")
        }
    }
}
