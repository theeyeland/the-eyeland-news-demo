//
//  JAJNTestCollectionViewCell.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 05.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit
import ReactiveSwift
import AlamofireImage

class ObjectMappingAppNewsCollectionViewCell: BaseCollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var gradientView: GradientView!

    var data: MutableProperty<ObjectMappingAppNewsModel>
        = MutableProperty<ObjectMappingAppNewsModel>(ObjectMappingAppNewsModel())

    deinit {
        log.theeyeland.debugDealloc(withObject: self, message: titleLabel.text ?? "")
    }

    override func setupSkin() {
        titleLabel.style.subTitle()
        gradientView.style.imageGradient()
        imageView.style.collectionCellApp()
    }

    override func setupUI() { }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        imageView.reactive.afImage <~ data.signal
            .flatMap(.latest, { [unowned self] (model) -> SignalProducer<URL?, Never> in
                return URL.storageURLProducer(relativePath: model.imageLink).take(during: self.reactive.lifetime)
        }).skipNil()

        if let titleLabel = titleLabel {
            titleLabel.reactive.text <~ data.signal.map {$0.title}
        }
    }
}
