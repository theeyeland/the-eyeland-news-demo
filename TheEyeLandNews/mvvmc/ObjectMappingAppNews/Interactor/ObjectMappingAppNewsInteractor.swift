//
//  GroupsInteractor.swift
//  MVVMC
//
//  Created by Adam Studenic on 28/07/2017.
//  Copyright © 2017 runtastic. All rights reserved.
//

import Foundation
import ReactiveSwift

protocol ObjectMappingAppNewsInteractorProtocol: BaseInteractorProtocol {

    var models: MutableProperty<[ObjectMappingAppNewsModel]> { get }
    var error: MutableProperty<RemoteDataProviderError> { get }
    var completed: Signal<Void, Never> { get }
    func fetch()
}

final class ObjectMappingAppNewsInteractor: BaseInteractor, ObjectMappingAppNewsInteractorProtocol {

    var models: MutableProperty<[ObjectMappingAppNewsModel]> = MutableProperty<[ObjectMappingAppNewsModel]>([])
    var error: MutableProperty<RemoteDataProviderError> = MutableProperty<RemoteDataProviderError>(.none)

    private let dataProvider: ObjectMappingAppNewsDataProvider

    var completed: Signal<Void, Never> {
        return dataProvider.fetch.completed
    }

    var router: TheEyeLandNews.Router = .starScalesPro

    init(dataProvider: ObjectMappingAppNewsDataProvider) {
        self.dataProvider = dataProvider
    }

    override func initialize() {
        models <~ self.dataProvider.fetch.values.on(value: {
            for model in $0 {
                log.debug(model.newsId)
                log.debug(model.title)
                log.debug(model.appstoreId)
                log.debug(model.imageLink)
            }
        })

        error <~ dataProvider.fetch.errors.on(value: {
            if case .dataRequestFailed(let message, _, _, _) = $0 {
                log.error("data request failed \(message)")
            } else if case .dataMappingFailed = $0 {
                log.error("FAILED to map data")
            }

            log.debug("localized description:  \($0.localizedDescription)")
        })
    }

    func fetch() {
        // fetch data from a data provider
        dataProvider.fetch.apply(router).start()
    }
}
