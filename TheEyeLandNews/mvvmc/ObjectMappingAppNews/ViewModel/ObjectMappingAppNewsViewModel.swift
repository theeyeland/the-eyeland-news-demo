//
//  ObjectMappingAppNewsViewModel.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 04.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import Foundation
import ReactiveSwift

final class ObjectMappingAppNewsViewModel: BaseViewModel {

    private let interactor: ObjectMappingAppNewsInteractorProtocol
    private let coordinator: ObjectMappingAppNewsCoordinatorProtocol

    private var newsModels: [ObjectMappingAppNewsModel] {
        return interactor.models.value
    }

    init(interactor: ObjectMappingAppNewsInteractorProtocol, coordinator: ObjectMappingAppNewsCoordinatorProtocol) {
        self.interactor = interactor
        self.coordinator = coordinator
    }

    override func initialize() {
        disposables += interactor.error.signal.observeValues { [unowned self] (error) in
            if case .noNetworkConnection = error {
                self.coordinator.showAlert(withTitle: TheEyeLandNews.Constants.Text.noInternetConncetionTitle,
                                           withMessage: TheEyeLandNews.Constants.Text.noInternetConncetionMessage)
            }
        }
    }

    func changed() -> SignalProducer<[ObjectMappingAppNewsModel], Never> {
        return interactor.models.producer
    }

    func completed() -> Signal<Void, Never> {
        return interactor.completed
    }

    func failed() -> Signal<RemoteDataProviderError, Never> {
        return interactor.error.signal
    }

    func fetch() {
        // interaction to be handled within calçot
        interactor.fetch()
    }

    func data(forRowAt index: Int) -> ObjectMappingAppNewsModel {
        let newsModel = newsModels[index]
        return newsModel
    }

    func show(at index: Int) {
        coordinator.present(news: newsModels[index])
    }

    func numberOfRows() -> Int {
        return newsModels.count
    }
}
