//
//  ObjectMappingAppNewsCoordinator.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 04.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit

protocol ObjectMappingAppNewsCoordinatorProtocol: ShowInAppStoreCoordinatorProtocol {}

final class ObjectMappingAppNewsCoordinator: ShowInAppStoreCoordinator, ObjectMappingAppNewsCoordinatorProtocol {}
