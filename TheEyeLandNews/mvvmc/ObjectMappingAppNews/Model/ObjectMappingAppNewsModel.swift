//
//  ObjectMappingAppNewsModel.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 01.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit
import ReactiveSwift
import Swinject
import Combine

class BaseObjectMappingAppNewsModel {

    var newsId: Int = 0
    var title: String = ""
    var appstoreId: Int = 0
    var imageLink: String = ""
    var obsolete: Bool = false
    var webLink: String = ""

    init() {}

    // if something can missing in json file it is better to write your own decoder or make entities optional as (var name: String? = nil)
    required convenience init(from decoder: Decoder) throws {
        self.init()
        let values = try decoder.container(keyedBy: CodingKeys.self)
        newsId = values.decode(.newsId, withDefault: 0)
        title = values.decode(.title, withDefault: "")
        appstoreId = values.decode(.appstoreId, withDefault: 0)
        imageLink = values.decode(.imageLink, withDefault: "")
        obsolete = values.decode(.obsolete, withDefault: true)
        webLink = values.decode(.webLink, withDefault: "")

    }
}

extension BaseObjectMappingAppNewsModel: Codable {

    enum CodingKeys: String, CodingKey {
        case newsId = "news_id"
        case title
        case appstoreId = "appstore_id"
        case imageLink = "image_link"
        case obsolete
        case webLink = "web_link"
    }
}

enum ObjectMappingAppNewsModelError: Error {
    case none
    case invalidAppStoreId
}

extension BaseObjectMappingAppNewsModel: AppURLCoordintatorProtocol {

    func webURL() -> URL? {
        return URL(string: TheEyeLandNews.Constants.baseWebURLPath + webLink)
    }

    func appStoreURL() throws -> URL {
        return try "\(TheEyeLandNews.Constants.appStoreLink)\(appstoreId)".asURL()
    }
}

class ObjectMappingAppNewsModel: BaseObjectMappingAppNewsModel {}
