//
//  ObjectMappingAppNewsCollectionViewController.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 05.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit
import ReactiveCocoa
import ReactiveSwift

class ObjectMappingAppNewsCollectionViewController: BaseViewController, CVCRefreshable {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!

    var viewModel: ObjectMappingAppNewsViewModel!

    deinit {
        disposables.dispose()
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Do any additional setup after loading the view.
        guard let viewModel = viewModel else {
            fatalError("view model \(ObjectMappingAppsViewModel.self) not set ")
        }

        disposables += viewModel.completed()
            .observeResult { [unowned self] _ in
                self.endRefreshing()
                self.collectionView.reloadData()
        }

        disposables += viewModel.failed()
            .observeResult({ [unowned self](_) in
                self.endRefreshing()
        })

        viewModel.fetch()

        title = TheEyeLandNews.Constants.Text.objectMappingAppNewsVCTitle

        disposables +=  refreshed().values.observeValues { [unowned self] (_) in
            self.viewModel.fetch()
        }

    }

    private func setupUI() {
        collectionView.delegate = self
        collectionView.dataSource = self
        // Register cell classes
        collectionView.register(ObjectMappingAppNewsCollectionViewCell.self, forCellWithReuseIdentifier: ~|ObjectMappingAppNewsCollectionViewCell.self)

        self.view.style.main()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        if let parent = parent {
//            parent.navigationItem.title = TheEyeLandNews.Constants.Text.objectMappingAppNewsVCTitle
//        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    public override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {

        coordinator.animate(alongsideTransition: { [unowned self](_) in

            if UIWindow.isPortrait {
                print("Portrait")
                // Do something
            } else {
                print("Anything But Portrait")
                // Do something else
            }

            self.flowLayout.invalidateLayout()

        }, completion: { (_) -> Void in
            print("rotation completed")
        })

        super.viewWillTransition(to: size, with: coordinator)
    }
}

extension ObjectMappingAppNewsCollectionViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.show(at: indexPath.item)
    }
}

extension ObjectMappingAppNewsCollectionViewController: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: ~|ObjectMappingAppNewsCollectionViewCell.self,
            for: indexPath) as? ObjectMappingAppNewsCollectionViewCell else {
            fatalError("Could not dequeue cell with identifier: \(ObjectMappingAppNewsCollectionViewCell.self)")
        }

        let data = viewModel.data(forRowAt: indexPath.row)
        cell.data.swap(data)

        return cell
    }
}

extension ObjectMappingAppNewsCollectionViewController: UICollectionViewDelegateFlowLayout {

    public func collectionView( _ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                                sizeForItemAt indexPath: IndexPath) -> CGSize {

        var value: CGFloat = 0.0

        if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
            let devider = collectionView.bounds.width > collectionView.bounds.height ? 4.0 : 3.0
            value = collectionView.bounds.width / CGFloat(devider)

            value -= flowLayout.minimumInteritemSpacing * (CGFloat(devider) - 1.0)
                + (flowLayout.sectionInset.left + flowLayout.sectionInset.right)/2.0
        } else {
            value = collectionView.bounds.width / CGFloat(2.0)

            value -= flowLayout.minimumInteritemSpacing
                + (flowLayout.sectionInset.left + flowLayout.sectionInset.right)/2.0
        }

        return CGSize(width: value, height: value)
    }
}
