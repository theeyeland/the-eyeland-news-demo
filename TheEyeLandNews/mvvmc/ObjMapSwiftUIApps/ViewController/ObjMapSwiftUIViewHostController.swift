//
//  ObjMapSwiftUIViewHostController.swift
//  TheEyeLandNews
//
//  Created by Zoltan Bognar The EyeLand on 03/11/2020.
//  Copyright © 2020 Mgr. Art. Ing. Zoltan Bognar. All rights reserved.
//

import Foundation
import SwiftUI

// Create a UIHostingController class that hosts your SwiftUI view
class SwiftUIViewHostingController: BaseViewController {

    var viewModel: ObjMapSwiftUIViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "SwiftUI Apps"

        let swiftUIController = UIHostingController(rootView: ObjMapSwiftUIView(viewModel: viewModel))

        addChild(swiftUIController)
        swiftUIController.view.translatesAutoresizingMaskIntoConstraints
            = false

        swiftUIController.view.frame = self.view.bounds
        view.addSubview(swiftUIController.view)

        NSLayoutConstraint.activate([
            swiftUIController.view.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            swiftUIController.view.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            swiftUIController.view.topAnchor.constraint(equalTo: view.topAnchor),
            swiftUIController.view.bottomAnchor.constraint(equalTo: view.bottomAnchor)
            ])

        swiftUIController.didMove(toParent: self)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if let navigationController = navigationController {
            navigationController.setNavigationBarHidden(true, animated: true)
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if let navigationController = navigationController {
            navigationController.setNavigationBarHidden(false, animated: true)
        }
    }
}
