//
//  ObjMapSwiftUIView.swift
//  TheEyeLandNews
//
//  Created by Zoltan Bognar The EyeLand on 03/11/2020.
//  Copyright © 2020 Mgr. Art. Ing. Zoltan Bognar. All rights reserved.
//

import SwiftUI
import Combine
import CombineFeedback
import CombineFeedbackUI

struct ObjMapSwiftUIView: View {

    @ObservedObject var viewModel: ObjMapSwiftUIViewModel

    var body: some View {
        NavigationView {
            List {
                ForEach(viewModel.models, id: \.applicationId) { model in
                    ObjMapCombineAppsModelRow(model: model)
                }
            }.navigationBarTitle("SwiftUI Apps")

            .onAppear {
                viewModel.fetch()
            }
        }
    }
}

struct ObjMapCombineAppsModelRow: View {

    @Environment(\.imageFetcher) var fetcher: ImageFetcher

    var model: ObjMapSwiftUIAppsModel

    private var appImage: AnyPublisher<UIImage, Never> {
        return URL.storageURLProducerCF(relativePath: model.imageLink).flatMap { (url) -> AnyPublisher<UIImage, Never> in
            if let url = url {
                return fetcher.image(for: url).eraseToAnyPublisher()
            }

            return Empty().eraseToAnyPublisher()
        }.eraseToAnyPublisher()

//        return model.imageURL().map(fetcher.image)
//            .default(to: Empty().eraseToAnyPublisher())
    }

    var body: some View {
        Perform { print("model title: \(model.title)") }
        NavigationLink(destination: ObjMapSwiftUIDetailViewController.initialize(model: model)) {
            HStack {
                AsyncImage(source: appImage, placeholder: UIImage()).frame(width: 78, height: 78)
                    .clipped().cornerRadius(10)
                VStack(alignment: .leading) {
                    Text("\(model.title)").font(.subheadline).bold()
                    Text("\(model.subtitle)").font(.footnote).foregroundColor(Color.gray)
                }
            }
        }
    }
}

extension View {

    func Perform(_ block: () -> Void) -> some View {
        block()
        return EmptyView()
    }
}

extension Optional {

    func `default`(to value: Wrapped) -> Wrapped {
        return self ?? value
    }
}

// struct ObjMapSwiftUIView_Previews: PreviewProvider {
//    static var previews: some View {
//        ObjMapSwiftUIView()
//    }
// }
