//
//  ObjMapSwiftUIViewModel.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 04.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import Foundation
import Combine

final class ObjMapSwiftUIViewModel: BaseViewModel, ObservableObject {

    private let interactor: ObjMapSwiftUIAppsInteractorProtocol
    private let coordinator: ObjMapCombineAppsCoordinatorProtocol

    @Published private(set) var models: [ObjMapSwiftUIAppsModel] = []
    @Published private(set) var isLoading = false

    init(interactor: ObjMapSwiftUIAppsInteractorProtocol, coordinator: ObjMapCombineAppsCoordinatorProtocol) {
        self.interactor = interactor
        self.coordinator = coordinator
    }

    override func initialize() {

        $models.sink {
            print($0)
        }.store(in: &cancellableSet)

        changed().assign(to: \.models, onUnowned: self).store(in: &cancellableSet)

        failed().sink { [unowned self] error in
            if case .noNetworkConnection = error {
                self.coordinator.showAlert(withTitle: TheEyeLandNews.Constants.Text.noInternetConncetionTitle,
                                           withMessage: TheEyeLandNews.Constants.Text.noInternetConncetionMessage)
            }
        }.store(in: &cancellableSet)

        completed().map { _ in return true }.assign(to: \.isLoading, onUnowned: self).store(in: &cancellableSet)
    }

    func changed() -> AnyPublisher<[ObjMapSwiftUIAppsModel], Never> {
        return interactor.models
    }

    func completed() -> AnyPublisher<Void, Never> {
        return interactor.completed
    }

    func failed() -> AnyPublisher<RemoteDataProviderError, Never> {
        return interactor.error
    }

    func fetch() {
        // interaction to be handled within calçot
        isLoading = true
        interactor.fetch()
    }

    func show(model: ObjMapSwiftUIAppsModel) {
        coordinator.present(model: model)
    }

    func show(at index: Int) {
        coordinator.present(model: models[index])
    }
}
