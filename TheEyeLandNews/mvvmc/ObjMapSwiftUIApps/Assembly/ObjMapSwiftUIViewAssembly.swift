//
//  ObjMapCombineAppsAssembly.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 29.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit
import Swinject
import SwinjectStoryboard

public class ObjMapSwiftUIViewAssembly: BaseAssembly {

    public override func assemble(container: Container) {

        // Navigation
        container.register(UINavigationController.self) { _ in UINavigationController() }

        // TabBarItem
        container.register(UITabBarItem.self, name: "arrow_back") { _ in
            UITabBarItem(title: TheEyeLandNews.Constants.Text.swiftUICombineVCTitle,
                         image: UIImage(named: "ic_arrow_back"),
                         selectedImage: UIImage(named: "ic_arrow_back"))
        }

        // Coordinator
        container.register(ObjMapCombineAppsCoordinator.self) { _ in ObjMapCombineAppsCoordinator() }
            .initCompleted { (resolver, coordinator: ObjMapCombineAppsCoordinator) in
                coordinator.navigationController = resolver.resolve(UINavigationController.self)!
        }

        // DataProvider
        container.register(ObjMapSwiftUIAppsDataProvider.self) { resolver in
            let rdp = RemoteDataCodableProvider<TheEyeLandNews.Router, ObjMapSwiftUIAppsModel>()
            rdp.networking = resolver.resolve(NetworkingProtocol.self)!
            return ObjMapSwiftUIAppsDataProvider(rdp)
        }

        // Interactor
        container.register(ObjMapSwiftUIAppsInteractorProtocol.self) { resolver in
            return ObjMapSwiftUIAppsInteractor(dataProvider: resolver.resolve(ObjMapSwiftUIAppsDataProvider.self)!)
        }.initCompleted { (_, interactor) in
            interactor.initialize()
        }

        // ViewModel
        container.register(ObjMapSwiftUIViewModel.self) { resolver in
            ObjMapSwiftUIViewModel(interactor: resolver.resolve(ObjMapSwiftUIAppsInteractorProtocol.self)!,
                                       coordinator: resolver.resolve(ObjMapCombineAppsCoordinator.self)!)
        }.initCompleted { (_, viewModel) in
            viewModel.initialize()
    }

        // ViewController
        container.storyboardInitCompleted(SwiftUIViewHostingController.self) { resolver, controller in
            controller.tabBarItem = resolver.resolve(UITabBarItem.self, name: "arrow_back")!
            controller.viewModel = resolver.resolve(ObjMapSwiftUIViewModel.self)!
            let navigation = resolver.resolve(UINavigationController.self)!
            navigation.viewControllers = [controller]
        }
    }
}
