//
//  ObjMapSwiftUIAppsDataProvider.swift
//  TheEyeLandNews
//
//  Created by Zoltan Bognar The EyeLand on 01/12/2020.
//  Copyright © 2020 Mgr. Art. Ing. Zoltan Bognar. All rights reserved.
//

import Foundation

typealias ObjMapSwiftUIAppsDataProvider = AnyRemoteDataCodableProvider<TheEyeLandNews.Router, ObjMapSwiftUIAppsModel>
