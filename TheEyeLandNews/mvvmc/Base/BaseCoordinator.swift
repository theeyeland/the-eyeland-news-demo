//
//  BaseCoordinator.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 08.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import Foundation
import Swinject

protocol BaseCoordinatorProtocol: class {

    func dismiss(animated: Bool)
    func showAlert(withTitle title: String, withMessage message: String)
}

class BaseCoordinator: CoordinatorDebugDeallocProtocol {

    weak var navigationController: UINavigationController?
    lazy var assembler: Assembler = Assembler.sharedAssembler
    lazy var notificationCenter: NotificationCenter = NotificationCenter.default

    deinit {
        log.theeyeland.debugDealloc(withObject: self)
    }

    func dismiss(animated: Bool = true) {
        if let navigationController = navigationController {
            navigationController.popViewController(animated: animated)
        }
    }

    func showAlert(withTitle title: String, withMessage message: String) {

        let title = title
        let message = message

        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: TheEyeLandNews.Constants.Common.ok, style: .default, handler: { _ in }))

        if let controller = navigationController?.topViewController {
            controller.present(alert, animated: true, completion: nil)
        }
    }
}
