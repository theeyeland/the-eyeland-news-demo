//
//  BaseInteractor.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 08.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import Foundation
import Combine

protocol BaseInteractorProtocol {

    func initialize()
}

class BaseInteractor: InteractorDebugDeallocProtocol {

    @available(iOS 13.0, *)
    lazy var cancellableSet: Set<AnyCancellable> = []

    deinit {
        log.theeyeland.debugDealloc(withObject: self)

        if #available(iOS 13.0, *) {
            cancellableSet.forEach {
                $0.cancel()
            }
        }
    }

    func initialize() {

    }
}
