//
//  BaseViewController.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 08.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit
import Combine
import ReactiveSwift

class BaseViewController: UIViewController, ViewControllerDebugDeallocProtocol {

    lazy var disposables = CompositeDisposable()
    lazy var cancellableSet: Set<AnyCancellable> = []

    deinit {
        log.theeyeland.debugDealloc(withObject: self)

        disposables.dispose()

        cancellableSet.forEach {
            $0.cancel()
        }
    }

    func initialize() {}
}
