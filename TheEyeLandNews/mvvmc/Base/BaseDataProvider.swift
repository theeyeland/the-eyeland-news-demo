//
//  BaseDataProvider.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 08.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import Foundation
import ReactiveSwift
import Combine

protocol BaseDataProviderProtocol {

    var isConnection: SignalProducer<Bool, RemoteDataProviderError> {get}
    @available(iOS 13.0, *)
    var isConnectionCF: AnyPublisher<Bool, RemoteDataProviderError> { get }
}

extension BaseDataProviderProtocol {

    var isConnection: SignalProducer<Bool, RemoteDataProviderError> {
        return SignalProducer<Bool, RemoteDataProviderError> { observer, _ in
            let network = NetworkManager.sharedInstance
            if network.reachability.connection != .unavailable {
                observer.send(value: true)
                observer.sendCompleted()
            } else {
                observer.send(error: .noNetworkConnection)
            }
        }
    }

    @available(iOS 13.0, *)
    var isConnectionCF: AnyPublisher<Bool, RemoteDataProviderError> {
        return Future<Bool, RemoteDataProviderError> { promises in
            let network = NetworkManager.sharedInstance
            if network.reachability.connection != .unavailable {
                promises(.success(true))
            } else {
                promises(.failure(.noNetworkConnection))
            }
        }.eraseToAnyPublisher()
    }
}

class BaseDataProvider: NSObject, BaseDataProviderProtocol, DataProviderDebugDeallocProtocol {

    lazy var disposables: CompositeDisposable = CompositeDisposable()

    deinit {
        disposables.dispose()
        log.theeyeland.debugDealloc(withObject: self)
    }
}
