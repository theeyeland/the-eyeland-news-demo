//
//  BaseTableViewCell.swift
//  Slovak Lines
//
//  Created by Zoltan Bognar The EyeLand on 18/12/2019.
//  Copyright © 2019 freevision s.r.o. All rights reserved.
//

import UIKit
import ReactiveSwift
import Combine

class BaseTableViewCell: UITableViewCell, ViewControllerCellDebugDeallocProtocol {

    lazy var disposables: CompositeDisposable = CompositeDisposable()
    lazy var cancellableSet: Set<AnyCancellable> = []

    lazy var skin: Skin = {
        if let skin = SkinStore.sharedInstance.skin {
            return skin
        } else {
            fatalError("skin is not created in SkinStore")
        }
    }()

    deinit {
        disposables.dispose()

        cancellableSet.forEach {
            $0.cancel()
        }

        log.theeyeland.debugDealloc(withObject: self)
    }

    func setupSkin() {
        fatalError("setupSkin() must override in child of BaseTableViewCell")

    }

    func setupUI() {
        fatalError("setupUI() must override in child of BaseTableViewCell")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        setupSkin()
        setupUI()
    }
}
