//
//  BaseCollectionViewCell.swift
//  TheEyeLandNews
//
//  Created by Zoltan Bognar The EyeLand on 03/02/2021.
//  Copyright © 2021 Mgr. Art. Ing. Zoltan Bognar. All rights reserved.
//

import UIKit
import ReactiveSwift
import Combine

class BaseCollectionViewCell: UICollectionViewCell, ViewControllerCellDebugDeallocProtocol {

    lazy var disposables: CompositeDisposable = CompositeDisposable()
    lazy var cancellableSet: Set<AnyCancellable> = []

    lazy var skin: Skin = {
        if let skin = SkinStore.sharedInstance.skin {
            return skin
        } else {
            fatalError("skin is not created in SkinStore")
        }
    }()

    deinit {
        disposables.dispose()

        cancellableSet.forEach {
            $0.cancel()
        }

        log.theeyeland.debugDealloc(withObject: self)
    }

    func setupSkin() {
        fatalError("setupSkin() must override in child of BaseTableViewCell")

    }

    func setupUI() {
        fatalError("setupUI() must override in child of BaseTableViewCell")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        setupSkin()
        setupUI()
    }
}
