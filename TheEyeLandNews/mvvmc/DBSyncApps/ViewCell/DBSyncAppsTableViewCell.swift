//
//  DBSyncTableViewCell.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 20.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit

import ReactiveSwift
import ReactiveCocoa

class DBSyncAppsTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var newsImageView: UIImageView!

    var data: MutableProperty<ApplicationObjectModel?> = MutableProperty<ApplicationObjectModel?>(nil)

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        titleLabel.reactive.attributedText <~ data.signal.skipNil().map { AttributedTextHelper.attributedText($0.title ?? "", $0.subtitle ?? "", "\n") }

        if let imageView = newsImageView {

            imageView.style.tableCellApp()

            imageView.reactive.afImage <~ data.signal.skipNil()
                .flatMap(.latest, { (model) -> SignalProducer<URL?, Never> in
                return URL.storageURLProducer(relativePath: model.imageLink).take(during: self.reactive.lifetime)
            }).skipNil()

//            imageView.reactive.afImage <~ data.signal.skipNil().map { (model) -> URL? in
//                return model.imageURL()
//            }.skipNil()
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
}
