//
//  DBSyncAppsViewModel.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 21.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import Foundation
import ReactiveSwift

final class DBSyncAppsViewModel: BaseViewModel {

    private let interactor: DBSyncAppsInteractorProtocol
    private let coordinator: DBSyncAppsCoordinatorProtocol

    init(interactor: DBSyncAppsInteractorProtocol, coordinator: DBSyncAppsCoordinatorProtocol) {
        self.interactor = interactor
        self.coordinator = coordinator
    }

    func completed() -> Signal<Void, Never> {
        return interactor.completed
    }

    func failed() -> Signal<RemoteDataProviderError, Never> {
        return interactor.error.signal
    }

    func fetch() {
        // interaction to be handled within calçot
        interactor.fetch()
    }

    func show(application: ApplicationObjectModel) {
        coordinator.present(application: application)
    }
}
