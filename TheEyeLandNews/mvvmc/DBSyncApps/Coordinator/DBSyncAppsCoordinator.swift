//
//  DBSyncAppsCoordinator.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 21.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit
import SwinjectStoryboard

protocol DBSyncAppsCoordinatorProtocol: BaseCoordinatorProtocol {

    func present(application: ApplicationObjectModel)
}

final class DBSyncAppsCoordinator: BaseCoordinator, DBSyncAppsCoordinatorProtocol {

    func present(application: ApplicationObjectModel) {

        log.debug("present: \(String(describing: application.title))")

        guard let navigationController = navigationController else { return }

        assembler.apply(assembly: DBSyncAppNewsAssembly(navigationController, application))

        let sb = assembler.resolver.resolve(SwinjectStoryboard.self, name: TheEyeLandNews.Constants.Storyboard.main)!

        guard let viewController = sb.instantiateViewController(withIdentifier: ~|DBSyncAppNewsViewController.self) as? DBSyncAppNewsViewController else {
            fatalError("Failed to create \(~|DBSyncAppNewsViewController.self)")
        }

        navigationController.pushViewController(viewController, animated: true)
    }
}
