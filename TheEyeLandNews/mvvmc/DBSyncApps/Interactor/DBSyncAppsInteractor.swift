//
//  DBSyncAppsInteractor.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 21.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import Foundation

import ReactiveSwift

protocol DBSyncAppsInteractorProtocol: BaseInteractorProtocol {

    var error: MutableProperty<RemoteDataProviderError> { get }
    var completed: Signal<Void, Never> { get }
    func fetch()
}

final class DBSyncAppsInteractor: BaseInteractor, DBSyncAppsInteractorProtocol {

    var error: MutableProperty<RemoteDataProviderError> = MutableProperty<RemoteDataProviderError>(.none)

    var completed: Signal<Void, Never> {
        return dataProvider.fetch.completed
    }

    private let dataProvider: DBSyncAppsDataProvider

    var router: TheEyeLandNews.Router = .dbSyncApps

    init(dataProvider: DBSyncAppsDataProvider) {
        self.dataProvider = dataProvider
    }

    override func initialize() {
        // fetchTests return no error here - added here for Demo and architecture style
        error <~ dataProvider.fetch.errors.on(value: {
            if case .dataRequestFailed(let message, _, _, _) = $0 {
                log.error("Data request failed: \(message)")
            } else if case .dbSyncFailed(let message, _) = $0 {
                log.error("Failed to sync data: \(message)")
            }

            log.debug("localized description:  \($0.localizedDescription)")
        })
    }

    func fetch() {
        // fetch data from a data provider
        self.dataProvider.fetch.apply(router).start()

    }
}
