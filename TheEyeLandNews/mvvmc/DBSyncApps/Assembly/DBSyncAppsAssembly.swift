//
//  DBSyncAppsAssembly.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 29.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit
import Swinject
import SwinjectStoryboard
import Sync

public class DBSyncAppsAssembly: BaseAssembly {

    public override func assemble(container: Container) {

        // Navigation
        container.register(UINavigationController.self) { _ in UINavigationController() }

        // TabBarItem
        container.register(UITabBarItem.self, name: "sync") { _ in
            UITabBarItem(title: "DB Sync", image: UIImage(named: "ic_sync"), selectedImage: UIImage(named: "ic_sync"))
        }

        // Coordinator
        container.register(DBSyncAppsCoordinator.self) { _ in DBSyncAppsCoordinator() }
            .initCompleted { (resolver, coordinator: DBSyncAppsCoordinator) in
                coordinator.navigationController = resolver.resolve(UINavigationController.self)!
        }

        // DataProvider
        container.register(DBSyncAppsDataProvider.self) { resolver in
            let provider = DBSyncAppsDataProvider()
            provider.database = resolver.resolve(DatabaseProtocol.self)!
            provider.networking = resolver.resolve(NetworkingProtocol.self)!
            provider.entityName = ~|ApplicationObjectModel.self
            return provider
        }

        // Interactor
        container.register(DBSyncAppsInteractorProtocol.self) { resolver in
            return DBSyncAppsInteractor(dataProvider: resolver.resolve(DBSyncAppsDataProvider.self)!)
        }.initCompleted { (_, interactor) in
            interactor.initialize()
        }

        // ViewModel
        container.register(DBSyncAppsViewModel.self) { resolver in
            DBSyncAppsViewModel(interactor: resolver.resolve(DBSyncAppsInteractorProtocol.self)!,
                                coordinator: resolver.resolve(DBSyncAppsCoordinator.self)!)
        }.initCompleted { (_, viewModel) in
            viewModel.initialize()
        }

        // ViewController
        container.storyboardInitCompleted(DBSyncAppsViewController.self) { resolver, controller in
            controller.tabBarItem = resolver.resolve(UITabBarItem.self, name: "sync")!
            controller.viewModel = resolver.resolve(DBSyncAppsViewModel.self)!
            controller.dataStack = resolver.resolve(DataStack.self)!
            let navigation = resolver.resolve(UINavigationController.self)!
            navigation.viewControllers = [controller]
        }
    }
}
