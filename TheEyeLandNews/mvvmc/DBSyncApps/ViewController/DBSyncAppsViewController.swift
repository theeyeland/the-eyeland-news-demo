//
//  DBSyncAppsViewController.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 20.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit

import Sync
import DATASource

import ReactiveCocoa
import ReactiveSwift

class DBSyncAppsViewController: BaseViewController, TVCRefreshable {

    @IBOutlet weak var tableView: UITableView!

    var viewModel: DBSyncAppsViewModel!

    var dataStack: DataStack!

    lazy var dataSource: DATASource = {
        let request: NSFetchRequest =  NSFetchRequest<NSFetchRequestResult>(entityName: ~|ApplicationObjectModel.self)
        request.sortDescriptors = [NSSortDescriptor(key: "applicationId", ascending: true)]

        let dataSource = DATASource(tableView: self.tableView, cellIdentifier: ~|DBSyncAppsTableViewCell.self,
                                    fetchRequest: request, mainContext: self.dataStack.mainContext,
                                    configuration: { cell, item, _ in

                                        guard let dbSyncCell = cell as? DBSyncAppsTableViewCell else {
                                            fatalError("Failed to cast \(~|DBSyncAppsTableViewCell.self)")
                                        }

                                        guard let dbSyncItem = item as? ApplicationObjectModel else {
                                            fatalError("Failed to cast \(~|ApplicationObjectModel.self)")
                                        }

                                        dbSyncCell.data.value = dbSyncItem
        })

        return dataSource
    }()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        title = TheEyeLandNews.Constants.Text.dbSyncAppsVCTitle
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = TheEyeLandNews.Constants.Text.dbSyncAppsVCTitle

        if let navigationController = navigationController {
            navigationController.navigationBar.prefersLargeTitles = true
        }
        // Uncoment for testing
//         dataStack.drop()

        self.tableView.dataSource = dataSource

        disposables += viewModel.completed().observeResult { [unowned self] _ in
            self.endRefreshing()
            self.dataSource.fetch()
            self.tableView.reloadData()
            self.logUpdatedDatabase()
        }

        disposables += viewModel.failed().observeResult({ [unowned self](_) in
            self.endRefreshing()
        })

        self.viewModel.fetch()

        disposables +=  refreshed().values.observeValues { [unowned self] (_) in
            self.viewModel.fetch()
        }
    }

    func logUpdatedDatabase() {
        let fetchRequest = NewsObjectModel.createFetchRequest()
        let sort = NSSortDescriptor(key: "newsId", ascending: true)
        fetchRequest.sortDescriptors = [sort]

        do {
            let objects = try dataStack.mainContext.fetch(fetchRequest)
            for object in objects {
                log.debug("database newsId = \(object.newsId)")
            }
        } catch {
            log.debug("Failed to fatch news objcets withError: \(error)")
        }
    }
}

extension DBSyncAppsViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        if let appObjectModel = dataSource.objectAtIndexPath(indexPath) as? ApplicationObjectModel {
            viewModel.show(application: appObjectModel)
        }
    }
}
