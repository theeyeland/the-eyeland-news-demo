//
//  ObjMapSwiftUIDetailViewController.swift
//  TheEyeLandNews
//
//  Created by Zoltan Bognar The EyeLand on 27/11/2020.
//  Copyright © 2020 Mgr. Art. Ing. Zoltan Bognar. All rights reserved.
//

import Foundation
import SwiftUI
import Swinject
import SwinjectStoryboard

struct ObjMapSwiftUIDetailViewController: UIViewControllerRepresentable {

    var model: ObjMapSwiftUIAppsModel

    // for swiftUI detail controller we are reusing the ObjMapCombineAppNewsCollectionViewController
    func makeUIViewController(context: Context) -> ObjMapCombineAppNewsCollectionViewController {

        Assembler.sharedAssembler.apply(assembly: ObjMapCombineAppNewsAssembly(UINavigationController(), model.router))

        let sb = Assembler.sharedAssembler.resolver.resolve(SwinjectStoryboard.self, name: TheEyeLandNews.Constants.Storyboard.objMapCombineApps)!

        guard let viewController = sb.instantiateViewController(
            withIdentifier: ~|ObjMapCombineAppNewsCollectionViewController.self) as? ObjMapCombineAppNewsCollectionViewController else {
                fatalError("Failed to create \(~|ObjMapCombineAppNewsCollectionViewController.self)")
        }

        return viewController
    }

    func updateUIViewController(_ viewController: ObjMapCombineAppNewsCollectionViewController, context: Context) {
    }

    static func initialize(model: ObjMapSwiftUIAppsModel) -> some View {
        return  ObjMapSwiftUIDetailViewController(model: model).navigationBarTitle(TheEyeLandNews.Constants.Text.swiftUICombineAppNewsVCTitle)
    }
}
