//
//  ShowInAppStoreCoordinator.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 22.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit

protocol ShowInAppStoreCoordinatorProtocol: BaseCoordinatorProtocol {

    func present(news: AppURLCoordintatorProtocol)
}

protocol AppURLCoordintatorProtocol {

    func appStoreURL() throws -> URL
    func webURL() -> URL?
    var obsolete: Bool {get}
}

class ShowInAppStoreCoordinator: BaseCoordinator, ShowInAppStoreCoordinatorProtocol {

    func present(news: AppURLCoordintatorProtocol) {

        if let url = news.webURL(), news.obsolete {
            UIApplication.shared.open(url, options: [:], completionHandler: { (_) in
                print("AppStore URL was presented")
            })
        } else {
            do {
                let url = try news.appStoreURL()

                UIApplication.shared.open(url, options: [:], completionHandler: { (_) in
                    print("AppStore URL was presented")
                })

            } catch ObjectMappingAppNewsModelError.invalidAppStoreId {
                print("invalid appstore id")
            } catch {
                print("cant create appstroe URL")
            }
        }
    }
}
