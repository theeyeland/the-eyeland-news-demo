//
//  DBSyncAppNewsViewController.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 22.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit

import Sync
import DATASource

import ReactiveCocoa
import ReactiveSwift

class DBSyncAppNewsViewController: BaseViewController, CVCRefreshable {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!

    var viewModel: DBSyncAppNewsViewModel!

    var dataStack: DataStack!

    lazy var dataSource: DATASource = {
        let request: NSFetchRequest =  NSFetchRequest<NSFetchRequestResult>(entityName: ~|NewsObjectModel.self)
        request.sortDescriptors = [NSSortDescriptor(key: "newsId", ascending: true)]

        let predicate: NSPredicate = NSPredicate(format: "application == %@", self.viewModel.application)
        request.predicate = predicate

        let dataSource = DATASource(collectionView: self.collectionView, cellIdentifier: ~|DBSyncAppNewsCollectionViewCell.self,
                                    fetchRequest: request, mainContext: self.dataStack.mainContext,
                                    configuration: { cell, item, _ in

                                        guard let dbSyncCell = cell as? DBSyncAppNewsCollectionViewCell else {
                                            fatalError("Failed to cast \(~|DBSyncAppNewsCollectionViewCell.self)")
                                        }

                                        guard let dbSyncItem = item as? NewsObjectModel else {
                                            fatalError("Failed to cast \(~|NewsObjectModel.self)")
                                        }

                                        dbSyncCell.data.value = dbSyncItem
        })

        return dataSource
    }()

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        title = TheEyeLandNews.Constants.Text.dbSyncAppNewsVCTitle
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        title = TheEyeLandNews.Constants.Text.dbSyncAppNewsVCTitle

        // Uncoment for testing
//        dataStack.drop()

        self.collectionView.dataSource = dataSource

        disposables += viewModel.completed()
            .observeResult { [unowned self] _ in
                self.endRefreshing()
                self.dataSource.fetch()
                self.collectionView.reloadData()
        }

        disposables += viewModel.failed()
            .observeResult({ [unowned self](_) in
                self.endRefreshing()
        })

        self.viewModel.fetch()

        disposables +=  refreshed().values.observeValues { [unowned self] (_) in
            self.viewModel.fetch()
        }
    }
}

extension DBSyncAppNewsViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let newsObjectModel = dataSource.objectAtIndexPath(indexPath) as? NewsObjectModel else {
            fatalError("Failed to cast \(~|NewsObjectModel.self)")
        }

        viewModel.show(news: newsObjectModel)
    }
}

extension DBSyncAppNewsViewController: UICollectionViewDelegateFlowLayout {

    public func collectionView( _ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                                sizeForItemAt indexPath: IndexPath) -> CGSize {

        var value: CGFloat = 0.0

        if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
            let devider = collectionView.bounds.width > collectionView.bounds.height ? 4.0 : 3.0
            value = collectionView.bounds.width / CGFloat(devider)

            value -= (flowLayout.minimumInteritemSpacing * (CGFloat(devider) - 1.0)
                + (flowLayout.sectionInset.left + flowLayout.sectionInset.right)/2.0)
        } else {
            value = collectionView.bounds.width / CGFloat(2.0)

            value -= (flowLayout.minimumInteritemSpacing
                + (flowLayout.sectionInset.left + flowLayout.sectionInset.right)/2.0)
        }

        return CGSize(width: value, height: value)
    }
}
