//
//  DBSyncAppNewsAssembly.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 29.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit

import Swinject
import SwinjectStoryboard
import Sync

class DBSyncAppNewsAssembly: BaseAssembly {

    public override func assemble(container: Container) {

        // Coordinator
        container.register(DBSyncAppNewsCoordinator.self) { _ in DBSyncAppNewsCoordinator() }
            .initCompleted { (resolver, coordinator: DBSyncAppNewsCoordinator) in
                coordinator.navigationController = self.navigationConroller
        }

        // DataProvider
        container.register(DBSyncAppNewsDataProvider.self) { resolver in
            let provider = DBSyncAppNewsDataProvider()
            provider.database = resolver.resolve(DatabaseProtocol.self)!
            provider.networking = resolver.resolve(NetworkingProtocol.self)!
            provider.entityName = ~|ApplicationObjectModel.self
            return provider
        }
        // Ineractor
        container.register(DBSyncAppNewsInteractorProtocol.self) { resolver in
            return DBSyncAppNewsInteractor(dataProvider: resolver.resolve(DBSyncAppNewsDataProvider.self)!)
        }.initCompleted { (_, interactor) in
            interactor.initialize()
        }

        // ViewModel
        container.register(DBSyncAppNewsViewModel.self) { resolver in
            let viewModel = DBSyncAppNewsViewModel(interactor: resolver.resolve(DBSyncAppNewsInteractorProtocol.self)!,
                                                coordinator: resolver.resolve(DBSyncAppNewsCoordinator.self)!)

            viewModel.application = self.application
            return viewModel
        }.initCompleted { (_, interactor) in
            interactor.initialize()
        }

        // Controller
        container.storyboardInitCompleted(DBSyncAppNewsViewController.self) { resolver, controller in
            controller.viewModel = resolver.resolve(DBSyncAppNewsViewModel.self)!
            controller.dataStack = resolver.resolve(DataStack.self)!
        }
    }

    private unowned let navigationConroller: UINavigationController
    private unowned let application: ApplicationObjectModel

    public init(_ navigationConroller: UINavigationController, _ application: ApplicationObjectModel) {
        self.navigationConroller = navigationConroller
        self.application = application

    }
}
