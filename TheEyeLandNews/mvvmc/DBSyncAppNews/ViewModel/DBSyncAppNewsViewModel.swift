//
//  DBSyncAppNewsViewModel.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 22.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import Foundation
import ReactiveSwift

final class DBSyncAppNewsViewModel: BaseViewModel {

    private let interactor: DBSyncAppNewsInteractorProtocol
    private let coordinator: DBSyncAppNewsCoordinatorProtocol

    var application: ApplicationObjectModel!

    init(interactor: DBSyncAppNewsInteractorProtocol, coordinator: DBSyncAppNewsCoordinatorProtocol) {
        self.interactor = interactor
        self.coordinator = coordinator
    }

    func completed() -> Signal<Void, Never> {
        return interactor.completed
    }

    func failed() -> Signal<RemoteDataProviderError, Never> {
        return interactor.error.signal
    }

    func fetch() {
        // interaction to be handled within calçot
        interactor.fetch()
    }

    func show(news: NewsObjectModel) {
        coordinator.present(news: news)
    }
}
