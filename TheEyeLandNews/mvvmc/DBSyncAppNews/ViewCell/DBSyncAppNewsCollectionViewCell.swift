//
//  DBSyncAppNewsCollectionViewCell.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 22.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit
import ReactiveSwift
import AlamofireImage

class DBSyncAppNewsCollectionViewCell: BaseCollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var gradientView: GradientView!

    // Example of adding local binding target
    public var afImage: BindingTarget<URL> {
        return imageView.reactive.makeBindingTarget { (view: UIImageView, url: URL) in
            view.image = nil
            view.af.setImage(withURL: url)
        }
    }

    var data: MutableProperty<NewsObjectModel?> = MutableProperty<NewsObjectModel?>(nil)

    deinit {
        log.theeyeland.debugDealloc(withObject: self, message: titleLabel.text ?? "")
    }

    override func setupSkin() {
        titleLabel.style.subTitle()
        imageView.style.collectionCellApp()
        gradientView.style.imageGradient()
    }
    override func setupUI() { }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        imageView.reactive.afImage <~ data.signal.skipNil()
            .flatMap(.latest, { [unowned self] (model) -> SignalProducer<URL?, Never> in
            return URL.storageURLProducer(relativePath: model.imageLink).take(during: self.reactive.lifetime)
        }).skipNil()

        if let titleLabel = titleLabel {
            titleLabel.reactive.text <~ data.signal.skipNil().map {$0.title}
        }

        self.reactive.prepareForReuse.observeValues { [unowned self] _ in
            self.imageView.image = nil
        }
    }
}
