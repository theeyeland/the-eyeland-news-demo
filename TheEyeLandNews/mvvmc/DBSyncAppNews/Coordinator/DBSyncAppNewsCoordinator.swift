//
//  DBSyncAppNewsCoordinator.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 22.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit

protocol DBSyncAppNewsCoordinatorProtocol: ShowInAppStoreCoordinatorProtocol {}

final class DBSyncAppNewsCoordinator: ShowInAppStoreCoordinator, DBSyncAppNewsCoordinatorProtocol {}
