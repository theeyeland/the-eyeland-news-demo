//
//  ObjMapCombineAppsInteractor.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 04.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import Foundation
import Combine

protocol ObjMapCombineAppsInteractorProtocol: BaseInteractorProtocol {

    var models: AnyPublisher<[ObjMapCombineAppsModel], Never> { get }
    var error: AnyPublisher<RemoteDataProviderError, Never> { get }
    var completed: AnyPublisher<Void, Never> { get }
    func fetch()
}

final class ObjMapCombineAppsInteractor: BaseInteractor, ObjMapCombineAppsInteractorProtocol {

    private let dataProvider: ObjMapCombineAppsDataProvider

    var models: AnyPublisher<[ObjMapCombineAppsModel], Never> {
        return self.dataProvider.fetchCF.values.eraseToAnyPublisher()
    }

    var error: AnyPublisher<RemoteDataProviderError, Never> {
        return self.dataProvider.fetchCF.errors.eraseToAnyPublisher()
    }

    var completed: AnyPublisher<Void, Never> {
        return self.dataProvider.fetchCF.completed
    }

    var router: TheEyeLandNews.Router = .objectMappingApps

    init(dataProvider: ObjMapCombineAppsDataProvider) {
        self.dataProvider = dataProvider
    }

    override func initialize() {
        self.dataProvider.fetchCF.values.sink { (data) in
            log.debug("\(data)")
        }.store(in: &cancellableSet)

        self.dataProvider.fetchCF.errors.sink {
            if case .dataRequestFailed(let message, _, _, _) = $0 {
                log.error("data request failed \(message)")
            } else if case .dataMappingFailed = $0 {
                log.error("FAILED to map data")
            }
            log.debug("localized description:  \($0.localizedDescription)")
        }.store(in: &cancellableSet)
    }

    func fetch() {
        // fetch data from a data provider
        self.dataProvider.fetchCF.apply(value: router)

    }
}
