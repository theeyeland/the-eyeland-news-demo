//
//  ObjMapCombineAppsAssembly.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 29.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit
import Swinject
import SwinjectStoryboard

public class ObjMapCombineAppsAssembly: BaseAssembly {

    public override func assemble(container: Container) {

        // Navigation
        container.register(UINavigationController.self) { _ in UINavigationController() }

        // TabBarItem
        container.register(UITabBarItem.self, name: "arrow_back") { _ in
            UITabBarItem(title: TheEyeLandNews.Constants.Text.objMapCombineVCTitle,
                         image: UIImage(named: "ic_arrow_back"),
                         selectedImage: UIImage(named: "ic_arrow_back"))
        }

        // Coordinator
        container.register(ObjMapCombineAppsCoordinator.self) { _ in ObjMapCombineAppsCoordinator() }
            .initCompleted { (resolver, coordinator: ObjMapCombineAppsCoordinator) in
                coordinator.navigationController = resolver.resolve(UINavigationController.self)!
        }

        // DataProvider
        container.register(ObjMapCombineAppsDataProvider.self) { resolver in
            let rdp = RemoteDataCodableProvider<TheEyeLandNews.Router, ObjMapCombineAppsModel>()
            rdp.networking = resolver.resolve(NetworkingProtocol.self)!
            return ObjMapCombineAppsDataProvider(rdp)
        }

        // Interactor
        container.register(ObjMapCombineAppsInteractorProtocol.self) { resolver in
            return ObjMapCombineAppsInteractor(dataProvider: resolver.resolve(ObjMapCombineAppsDataProvider.self)!)
        }.initCompleted { (_, interactor) in
            interactor.initialize()
        }

        // ViewModel
        container.register(ObjMapCombineAppsViewModel.self) { resolver in
            ObjMapCombineAppsViewModel(interactor: resolver.resolve(ObjMapCombineAppsInteractorProtocol.self)!,
                                       coordinator: resolver.resolve(ObjMapCombineAppsCoordinator.self)!)
        }.initCompleted { (_, viewModel) in
            viewModel.initialize()
    }

        // ViewController
        container.storyboardInitCompleted(ObjMapCombineAppsViewController.self) { resolver, controller in
            controller.tabBarItem = resolver.resolve(UITabBarItem.self, name: "arrow_back")!
            controller.viewModel = resolver.resolve(ObjMapCombineAppsViewModel.self)!
            let navigation = resolver.resolve(UINavigationController.self)!
            navigation.viewControllers = [controller]
        }
    }
}
