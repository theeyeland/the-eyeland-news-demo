//
//  ObjMapCombineAppsCoordinator.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 04.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit
import Swinject
import SwinjectStoryboard

protocol ObjMapCombineAppsCoordinatorProtocol: BaseCoordinatorProtocol {

    func present(model: BaseObjectMappingAppsModel)
}

final class ObjMapCombineAppsCoordinator: BaseCoordinator, ObjMapCombineAppsCoordinatorProtocol {

    func present(model: BaseObjectMappingAppsModel) {

        log.debug("present: \(model.title)")

        guard let navigationController = navigationController else { return }

        assembler.apply(assembly: ObjMapCombineAppNewsAssembly(navigationController, model.router))

        let sb = assembler.resolver.resolve(SwinjectStoryboard.self, name: TheEyeLandNews.Constants.Storyboard.objMapCombineApps)!

        guard let viewController = sb.instantiateViewController(
            withIdentifier: ~|ObjMapCombineAppNewsCollectionViewController.self) as? ObjMapCombineAppNewsCollectionViewController else {
                fatalError("Failed to create \(~|ObjMapCombineAppNewsCollectionViewController.self)")
        }

        navigationController.pushViewController(viewController, animated: true)
    }
}
