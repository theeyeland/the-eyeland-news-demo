//
//  ViewController.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 01.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit

class ObjMapCombineAppsViewController: BaseViewController, TVCRefreshable {

    @IBOutlet weak var tableView: UITableView!

    var viewModel: ObjMapCombineAppsViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        if let navigationController = navigationController {
            navigationController.navigationBar.prefersLargeTitles = true
        }

        guard let viewModel = viewModel else {
            fatalError("view model \(ObjMapCombineAppsViewModel.self) not set ")
        }

        viewModel.completed().sink { [unowned self] _ in
            self.endRefreshing()
            self.tableView.reloadData()
        }.store(in: &cancellableSet)

        viewModel.failed().sink { [unowned self] _ in
            self.endRefreshing()
        }.store(in: &cancellableSet)

        viewModel.fetch()

        title = TheEyeLandNews.Constants.Text.objMapCombineVCTitle

        refreshedCF().sink { [unowned self] _ in
            self.viewModel.fetch()
        }.store(in: &cancellableSet)
    }
}

extension ObjMapCombineAppsViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: ~|ObjMapCombineAppsTableViewCell.self,
                                                       for: indexPath) as? ObjMapCombineAppsTableViewCell else {

            fatalError("Could not dequeue cell with identifier: \(ObjMapCombineAppsTableViewCell.self)")
        }

        cell.data = viewModel.data(forRowAt: indexPath.row)

        return cell
    }
}

extension ObjMapCombineAppsViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        viewModel.show(at: indexPath.row)
    }
}
