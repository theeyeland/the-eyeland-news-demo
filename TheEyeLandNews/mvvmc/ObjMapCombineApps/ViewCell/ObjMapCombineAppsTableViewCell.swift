//
//  ObjMapCombineAppsTableViewCell.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 04.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit
import Combine

class ObjMapCombineAppsTableViewCell: BaseTableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var newsImageView: UIImageView!

    @Published var data: ObjMapCombineAppsModel = ObjMapCombineAppsModel()

    override func setupSkin() {
        if let imageView = newsImageView {
            imageView.style.tableCellApp()
        }
    }

    override func setupUI() {
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        $data.map { AttributedTextHelper.attributedText($0.title, $0.subtitle, "\n") }
            .assign(to: \.attributedText, onUnowned: self.titleLabel)
            .store(in: &cancellableSet)

        if let imageView = newsImageView {
            $data.flatMap { model -> AnyPublisher<URL?, Never> in
                return URL.storageURLProducerCF(relativePath: model.imageLink)
            }.skipNil().sink { [unowned imageView] url in
                imageView.af.setImage(withURL: url)
            }.store(in: &cancellableSet)
        }

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
    }

}
