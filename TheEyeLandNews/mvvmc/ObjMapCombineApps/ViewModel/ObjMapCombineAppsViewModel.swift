//
//  ObjMapCombineAppsViewModel.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 04.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import Foundation
import Combine

final class ObjMapCombineAppsViewModel: BaseViewModel {

    private let interactor: ObjMapCombineAppsInteractorProtocol
    private let coordinator: ObjMapCombineAppsCoordinatorProtocol

    private var models: [ObjMapCombineAppsModel] = []

    init(interactor: ObjMapCombineAppsInteractorProtocol, coordinator: ObjMapCombineAppsCoordinatorProtocol) {
        self.interactor = interactor
        self.coordinator = coordinator
    }

    override func initialize() {

        interactor.models.sink {
            print($0)
        }.store(in: &cancellableSet)

        interactor.models.assign(to: \.models, onUnowned: self).store(in: &cancellableSet)

        interactor.error.sink { [unowned self] error in
            if case .noNetworkConnection = error {
                self.coordinator.showAlert(withTitle: TheEyeLandNews.Constants.Text.noInternetConncetionTitle,
                                           withMessage: TheEyeLandNews.Constants.Text.noInternetConncetionMessage)
            }
        }.store(in: &cancellableSet)
    }

    func changed() -> AnyPublisher<[ObjMapCombineAppsModel], Never> {
        return interactor.models
    }

    func completed() -> AnyPublisher<Void, Never> {
        return interactor.completed
    }

    func failed() -> AnyPublisher<RemoteDataProviderError, Never> {
        return interactor.error
    }

    func fetch() {
        // interaction to be handled within calçot
        interactor.fetch()
    }

    func data(forRowAt index: Int) -> ObjMapCombineAppsModel {
        let testModel = models[index]
        return testModel
    }

    func show(at index: Int) {
        coordinator.present(model: models[index])
    }

    func numberOfRows() -> Int {
        return models.count
    }
}
