//
//  ObjMapCombineAppNewsViewModel.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 04.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import Foundation
import Combine

final class ObjMapCombineAppNewsViewModel: BaseViewModel {

    private let interactor: ObjMapCombineAppNewsInteractorProtocol
    private let coordinator: ObjMapCombineAppNewsCoordinatorProtocol

    private var models: [ObjMapCombineAppNewsModel] = []

    init(interactor: ObjMapCombineAppNewsInteractorProtocol, coordinator: ObjMapCombineAppNewsCoordinatorProtocol) {
        self.interactor = interactor
        self.coordinator = coordinator
    }

    override func initialize() {

        interactor.models.sink {
            print($0)
        }.store(in: &cancellableSet)

        interactor.models.assign(to: \.models, onUnowned: self).store(in: &cancellableSet)

        interactor.error.sink { [unowned self] error in
            if case .noNetworkConnection = error {
                self.coordinator.showAlert(withTitle: TheEyeLandNews.Constants.Text.noInternetConncetionTitle,
                                           withMessage: TheEyeLandNews.Constants.Text.noInternetConncetionMessage)
            }
        }.store(in: &cancellableSet)
    }

    func changed() -> AnyPublisher<[ObjMapCombineAppNewsModel], Never> {
        return interactor.models
    }

    func completed() -> AnyPublisher<Void, Never> {
        return interactor.completed
    }

    func failed() -> AnyPublisher<RemoteDataProviderError, Never> {
        return interactor.error
    }

    func fetch() {
        // interaction to be handled within calçot
        interactor.fetch()
    }

    func data(forRowAt index: Int) -> ObjMapCombineAppNewsModel {
        let newsModel = models[index]
        return newsModel
    }

    func show(at index: Int) {
        coordinator.present(news: models[index])
    }

    func numberOfRows() -> Int {
        return models.count
    }
}
