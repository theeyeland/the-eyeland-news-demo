//
//  ObjMapCombineAppNewsAssembly.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 29.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit
import Swinject
import SwinjectStoryboard

class ObjMapCombineAppNewsAssembly: BaseAssembly {

    public override func assemble(container: Container) {

        // Coordinator
        container.register(ObjMapCombineAppNewsCoordinator.self) { _ in ObjMapCombineAppNewsCoordinator() }
            .initCompleted { (resolver, coordinator: ObjMapCombineAppNewsCoordinator) in
                if let navigationConroller = self.navigationConroller {
                    coordinator.navigationController = navigationConroller
                }
        }

        // DataProvider
        container.register(ObjMapCombineAppNewsDataProvider.self) { resolver in
            let rdp = RemoteDataCodableProvider<TheEyeLandNews.Router, ObjMapCombineAppNewsModel>()
            rdp.networking = resolver.resolve(NetworkingProtocol.self)!
            return ObjMapCombineAppNewsDataProvider(rdp)
        }

        // Ineractor
        container.register(ObjMapCombineAppNewsInteractorProtocol.self) { resolver in
            let interactor = ObjMapCombineAppNewsInteractor(dataProvider: resolver.resolve(ObjMapCombineAppNewsDataProvider.self)!)
            interactor.router = self.router
            return interactor
        }.initCompleted { (_, interactor) in
            interactor.initialize()
        }

        // ViewModel
        container.register(ObjMapCombineAppNewsViewModel.self) { resolver in
            ObjMapCombineAppNewsViewModel(interactor: resolver.resolve(ObjMapCombineAppNewsInteractorProtocol.self)!,
                                       coordinator: resolver.resolve(ObjMapCombineAppNewsCoordinator.self)!)
        }.initCompleted { (_, viewModel) in
            viewModel.initialize()
        }

        // Controller
        container.storyboardInitCompleted(ObjMapCombineAppNewsCollectionViewController.self) { resolver, contorller in
            contorller.viewModel = resolver.resolve(ObjMapCombineAppNewsViewModel.self)!
        }
    }

    private weak var navigationConroller: UINavigationController?
    private var router: TheEyeLandNews.Router = .starScalesPro

    public init(_ navigationConroller: UINavigationController?, _ router: String) {
        self.navigationConroller = navigationConroller

        if let mainRouter = TheEyeLandNews.Router(rawValue: router) {
            self.router = mainRouter
        } else {
            log.error("TheEyeLandNews.Router can't be initialized from ObjectMappingAppsModel.router = \(router)")
        }
    }
}
