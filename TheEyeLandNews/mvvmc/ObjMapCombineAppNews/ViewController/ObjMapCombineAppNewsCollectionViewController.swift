//
//  ObjMapCombineAppNewsCollectionViewController.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 05.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit
import Combine

class ObjMapCombineAppNewsCollectionViewController: BaseViewController, CVCRefreshable {

    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var flowLayout: UICollectionViewFlowLayout!

    var viewModel: ObjMapCombineAppNewsViewModel!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Do any additional setup after loading the view.
        guard let viewModel = viewModel else {
            fatalError("view model \(ObjMapCombineAppNewsViewModel.self) not set ")
        }

        viewModel.completed().sink { [unowned self] _ in
            self.endRefreshing()
            self.collectionView.reloadData()
        }.store(in: &cancellableSet)

        viewModel.failed().sink { [unowned self] _ in
            self.endRefreshing()
        }.store(in: &cancellableSet)

        viewModel.fetch()

        title = TheEyeLandNews.Constants.Text.objMapCombineAppNewsVCTitle

        refreshedCF().sink { [unowned self] _ in
            self.viewModel.fetch()
        }.store(in: &cancellableSet)

    }

    private func setupUI() {
        collectionView.delegate = self
        collectionView.dataSource = self
        // Register cell classes
        collectionView.register(ObjMapCombineAppNewsCollectionViewCell.self, forCellWithReuseIdentifier: ~|ObjMapCombineAppNewsCollectionViewCell.self)

        self.view.style.main()
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
//        if let parent = parent {
//            parent.navigationItem.title = TheEyeLandNews.Constants.Text.ObjMapCombineAppNewsVCTitle
//        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    public override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {

        coordinator.animate(alongsideTransition: { [unowned self](_) in

            if UIWindow.isPortrait {
                print("Portrait")
                // Do something
            } else {
                print("Anything But Portrait")
                // Do something else
            }

            self.flowLayout.invalidateLayout()

        }, completion: { (_) -> Void in
            print("rotation completed")
        })

        super.viewWillTransition(to: size, with: coordinator)
    }
}

extension ObjMapCombineAppNewsCollectionViewController: UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        viewModel.show(at: indexPath.item)
    }
}

extension ObjMapCombineAppNewsCollectionViewController: UICollectionViewDataSource {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

        guard let cell = collectionView.dequeueReusableCell(
            withReuseIdentifier: ~|ObjMapCombineAppNewsCollectionViewCell.self,
            for: indexPath) as? ObjMapCombineAppNewsCollectionViewCell else {
            fatalError("Could not dequeue cell with identifier: \(ObjMapCombineAppNewsCollectionViewCell.self)")
        }

        let data = viewModel.data(forRowAt: indexPath.row)
        cell.data = data

        return cell
    }
}

extension ObjMapCombineAppNewsCollectionViewController: UICollectionViewDelegateFlowLayout {

    public func collectionView( _ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout,
                                sizeForItemAt indexPath: IndexPath) -> CGSize {

        var value: CGFloat = 0.0

        if UIDevice.current.userInterfaceIdiom == UIUserInterfaceIdiom.pad {
            let devider = collectionView.bounds.width > collectionView.bounds.height ? 4.0 : 3.0
            value = collectionView.bounds.width / CGFloat(devider)

            value -= flowLayout.minimumInteritemSpacing * (CGFloat(devider) - 1.0)
                + (flowLayout.sectionInset.left + flowLayout.sectionInset.right)/2.0
        } else {
            value = collectionView.bounds.width / CGFloat(2.0)

            value -= flowLayout.minimumInteritemSpacing
                + (flowLayout.sectionInset.left + flowLayout.sectionInset.right)/2.0
        }

        return CGSize(width: value, height: value)
    }
}
