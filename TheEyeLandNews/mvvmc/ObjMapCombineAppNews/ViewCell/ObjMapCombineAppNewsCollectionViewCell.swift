//
//  JAJNTestCollectionViewCell.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 05.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit
import AlamofireImage
import ReactiveSwift
import Combine

class ObjMapCombineAppNewsCollectionViewCell: BaseCollectionViewCell {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var gradientView: GradientView!

    // Example of adding local binding target
    public var afImage: BindingTarget<URL> {
        return imageView.reactive.makeBindingTarget { (view: UIImageView, url: URL) in
            view.image = nil
            view.af.setImage(withURL: url)
        }
    }

    @Published var data: ObjMapCombineAppNewsModel = ObjMapCombineAppNewsModel()

    deinit {
        log.theeyeland.debugDealloc(withObject: self, message: titleLabel.text ?? "")
    }

    override func setupSkin() {
        titleLabel.style.subTitle()
        gradientView.style.imageGradient()
        imageView.style.collectionCellApp()
    }

    override func setupUI() {
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        if let _ = imageView {
            $data.flatMap({ (model) -> AnyPublisher<URL?, Never> in
                return URL.storageURLProducerCF(relativePath: model.imageLink)
            }).skipNil().sink { [unowned self] url in
                self.imageView.af.setImage(withURL: url)
            }.store(in: &cancellableSet)
        }

        if let _ = titleLabel {
            $data.map { $0.title }
                .assign(to: \.text, onUnowned: self.titleLabel)
                .store(in: &cancellableSet)
        }
    }
}
