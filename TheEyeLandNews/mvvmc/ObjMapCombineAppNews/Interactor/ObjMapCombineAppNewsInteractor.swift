//
//  GroupsInteractor.swift
//  MVVMC
//
//  Created by Adam Studenic on 28/07/2017.
//  Copyright © 2017 runtastic. All rights reserved.
//

import Foundation
import Combine

protocol ObjMapCombineAppNewsInteractorProtocol: BaseInteractorProtocol {

    var models: AnyPublisher<[ObjMapCombineAppNewsModel], Never> { get }
    var error: AnyPublisher<RemoteDataProviderError, Never> { get }
    var completed: AnyPublisher<Void, Never> { get }
    func fetch()
}

final class ObjMapCombineAppNewsInteractor: BaseInteractor, ObjMapCombineAppNewsInteractorProtocol {

    private let dataProvider: ObjMapCombineAppNewsDataProvider

    var models: AnyPublisher<[ObjMapCombineAppNewsModel], Never> {
        return self.dataProvider.fetchCF.values.eraseToAnyPublisher()
    }

    var error: AnyPublisher<RemoteDataProviderError, Never> {
        return self.dataProvider.fetchCF.errors.eraseToAnyPublisher()
    }

    var completed: AnyPublisher<Void, Never> {
        return self.dataProvider.fetchCF.completed
    }

    var router: TheEyeLandNews.Router = .starScalesPro

    init(dataProvider: ObjMapCombineAppNewsDataProvider) {
        self.dataProvider = dataProvider
    }

    override func initialize() {
        self.dataProvider.fetchCF.values.sink { (data) in
            log.debug("\(data)")
        }.store(in: &cancellableSet)

        self.dataProvider.fetchCF.errors.sink {
            if case .dataRequestFailed(let message, _, _, _) = $0 {
                log.error("data request failed \(message)")
            } else if case .dataMappingFailed = $0 {
                log.error("FAILED to map data")
            }
            log.debug("localized description:  \($0.localizedDescription)")
        }.store(in: &cancellableSet)
    }

    func fetch() {
        // fetch data from a data provider
        dataProvider.fetchCF.apply(value: router)
    }
}
