//
//  ObjectMappingAppsTableViewCell.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 04.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit
import ReactiveSwift
import ReactiveCocoa

class ObjectMappingAppsTableViewCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var newsImageView: UIImageView!

    var data: MutableProperty<ObjectMappingAppsModel> = MutableProperty<ObjectMappingAppsModel>(ObjectMappingAppsModel())

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code

        titleLabel.reactive.attributedText <~ data.signal.map { AttributedTextHelper.attributedText($0.title, $0.subtitle, "\n") }

        if let imageView = newsImageView {
            imageView.style.tableCellApp()

            imageView.reactive.afImage <~ data.signal
                .flatMap(.latest, { [unowned self] model -> SignalProducer<URL?, Never> in
                return URL.storageURLProducer(relativePath: model.imageLink).take(during: self.reactive.lifetime)
            }).skipNil()
        }

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
