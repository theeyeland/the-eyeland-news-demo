//
//  ObjectMappingAppsViewModel.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 04.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import Foundation
import ReactiveSwift

final class ObjectMappingAppsViewModel: BaseViewModel {

    private let interactor: ObjectMappingAppsInteractorProtocol
    private let coordinator: ObjectMappingAppsCoordinatorProtocol

    private var models: [ObjectMappingAppsModel] {
        return interactor.models.value
    }

    init(interactor: ObjectMappingAppsInteractorProtocol, coordinator: ObjectMappingAppsCoordinatorProtocol) {
        self.interactor = interactor
        self.coordinator = coordinator
    }

    override func initialize() {
        disposables += interactor.error.signal.observeValues { [unowned self] (error) in
            if case .noNetworkConnection = error {
                self.coordinator.showAlert(withTitle: TheEyeLandNews.Constants.Text.noInternetConncetionTitle,
                                           withMessage: TheEyeLandNews.Constants.Text.noInternetConncetionMessage)
            }
        }
    }

    func changed() -> SignalProducer<[ObjectMappingAppsModel], Never> {
        return interactor.models.producer
    }

    func completed() -> Signal<Void, Never> {
        return interactor.completed
    }

    func failed() -> Signal<RemoteDataProviderError, Never> {
        return interactor.error.signal
    }

    func fetch() {
        // interaction to be handled within calçot
        interactor.fetch()
    }

    func data(forRowAt index: Int) -> ObjectMappingAppsModel {
        let testModel = models[index]
        return testModel
    }

    func show(at index: Int) {
        coordinator.present(model: models[index])
    }

    func numberOfRows() -> Int {
        return models.count
    }
}
