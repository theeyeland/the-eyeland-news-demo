//
//  ObjectMappingAppsCoordinator.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 04.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit
import Swinject
import SwinjectStoryboard

protocol ObjectMappingAppsCoordinatorProtocol: BaseCoordinatorProtocol {

    func present(model: ObjectMappingAppsModel)
}

final class ObjectMappingAppsCoordinator: BaseCoordinator, ObjectMappingAppsCoordinatorProtocol {

    func present(model: ObjectMappingAppsModel) {

        log.debug("present: \(model.title)")

        guard let navigationController = navigationController else { return }

        assembler.apply(assembly: ObjectMappingAppNewsAssembly(navigationController, model.router))

        let sb = assembler.resolver.resolve(SwinjectStoryboard.self, name: TheEyeLandNews.Constants.Storyboard.main)!

        guard let viewController = sb.instantiateViewController(
            withIdentifier: ~|ObjectMappingAppNewsCollectionViewController.self) as? ObjectMappingAppNewsCollectionViewController else {
                fatalError("Failed to create \(~|ObjectMappingAppNewsCollectionViewController.self)")
        }

        navigationController.pushViewController(viewController, animated: true)
    }
}
