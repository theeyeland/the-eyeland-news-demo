//
//  ViewController.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 01.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit

import ReactiveSwift
import ReactiveCocoa

class ObjectMappingAppsViewController: UIViewController, TVCRefreshable {

    let disposables = CompositeDisposable()

    @IBOutlet weak var tableView: UITableView!

    var viewModel: ObjectMappingAppsViewModel!

    deinit {
        disposables.dispose()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.

        if let navigationController = navigationController {
            navigationController.navigationBar.prefersLargeTitles = true
        }

        guard let viewModel = viewModel else {
            fatalError("view model \(ObjectMappingAppsViewModel.self) not set ")
        }

        disposables += viewModel.completed().observeResult { [unowned self] _ in
            self.endRefreshing()
            self.tableView.reloadData()
        }

        disposables += viewModel.failed().observeResult({ [unowned self](_) in
            self.endRefreshing()
        })

        viewModel.fetch()

        title = TheEyeLandNews.Constants.Text.objectMappingAppsVCTitle

        disposables +=  refreshed().values.observeValues { [unowned self] (_) in
            self.viewModel.fetch()
        }
    }
}

extension ObjectMappingAppsViewController: UITableViewDataSource {

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfRows()
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        guard let cell = tableView.dequeueReusableCell(withIdentifier: ~|ObjectMappingAppsTableViewCell.self,
                                                       for: indexPath) as? ObjectMappingAppsTableViewCell else {

            fatalError("Could not dequeue cell with identifier: \(ObjectMappingAppsTableViewCell.self)")
        }

        let data = viewModel.data(forRowAt: indexPath.row)
        cell.data.swap(data)

        return cell
    }
}

extension ObjectMappingAppsViewController: UITableViewDelegate {

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)

        viewModel.show(at: indexPath.row)
    }
}
