//
//  ObjectMappingAppsAssembly.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 29.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit
import Swinject
import SwinjectStoryboard

public class ObjectMappingAppsAssembly: BaseAssembly {

    public override func assemble(container: Container) {

        // Navigation
        container.register(UINavigationController.self) { _ in UINavigationController() }

        // TabBarItem
        container.register(UITabBarItem.self, name: "arrow_back") { _ in
            UITabBarItem(title: TheEyeLandNews.Constants.Text.objectMappingAppsVCTitle,
                         image: UIImage(named: "ic_arrow_back"),
                         selectedImage: UIImage(named: "ic_arrow_back"))
        }

        // Coordinator
        container.register(ObjectMappingAppsCoordinator.self) { _ in ObjectMappingAppsCoordinator() }
            .initCompleted { (resolver, coordinator: ObjectMappingAppsCoordinator) in
                coordinator.navigationController = resolver.resolve(UINavigationController.self)!
        }

        // DataProvider
        container.register(ObjectMappingAppsDataProvider.self) { resolver in
            let rdp = RemoteDataCodableProvider<TheEyeLandNews.Router, ObjectMappingAppsModel>()
            rdp.networking = resolver.resolve(NetworkingProtocol.self)!
            return ObjectMappingAppsDataProvider(rdp)
        }

        // Interactor
        container.register(ObjectMappingAppsInteractorProtocol.self) { resolver in
            return ObjectMappingAppsInteractor(dataProvider: resolver.resolve(ObjectMappingAppsDataProvider.self)!)
        }.initCompleted { (_, interactor) in
            interactor.initialize()
        }

        // ViewModel
        container.register(ObjectMappingAppsViewModel.self) { resolver in
            ObjectMappingAppsViewModel(interactor: resolver.resolve(ObjectMappingAppsInteractorProtocol.self)!,
                                       coordinator: resolver.resolve(ObjectMappingAppsCoordinator.self)!)
        }.initCompleted { (_, viewModel) in
            viewModel.initialize()
        }

        // ViewController
        container.storyboardInitCompleted(ObjectMappingAppsViewController.self) { resolver, controller in
            controller.tabBarItem = resolver.resolve(UITabBarItem.self, name: "arrow_back")!
            controller.viewModel = resolver.resolve(ObjectMappingAppsViewModel.self)!
            let navigation = resolver.resolve(UINavigationController.self)!
            navigation.viewControllers = [controller]
        }
    }
}
