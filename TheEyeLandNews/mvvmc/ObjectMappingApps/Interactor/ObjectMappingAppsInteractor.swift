//
//  ObjectMappingAppsInteractor.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 04.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import Foundation
import ReactiveSwift
import Combine

protocol ObjectMappingAppsInteractorProtocol: BaseInteractorProtocol {

    var models: MutableProperty<[ObjectMappingAppsModel]> { get }
    var error: MutableProperty<RemoteDataProviderError> { get }
    var completed: Signal<Void, Never> { get }
    func fetch()
}

final class ObjectMappingAppsInteractor: BaseInteractor, ObjectMappingAppsInteractorProtocol {

    var models: MutableProperty<[ObjectMappingAppsModel]> = MutableProperty<[ObjectMappingAppsModel]>([])
    var error: MutableProperty<RemoteDataProviderError> = MutableProperty<RemoteDataProviderError>(.none)

    private let dataProvider: ObjectMappingAppsDataProvider

    var completed: Signal<Void, Never> {
        return dataProvider.fetch.completed
    }

    var router: TheEyeLandNews.Router = .objectMappingApps

    init(dataProvider: ObjectMappingAppsDataProvider) {
        self.dataProvider = dataProvider
    }

    override func initialize() {

        models <~ self.dataProvider.fetch.values
        // fetchTests return no error here - added here for Demo and architecture style
        error <~ dataProvider.fetch.errors.on(value: {
            if case .dataRequestFailed(let message, _, _, _) = $0 {
                log.error("Data request failed: \(message)")
            } else if case .dbSyncFailed(let message, _) = $0 {
                log.error("Failed to sync data: \(message)")
            }

            log.debug("localized description:  \($0.localizedDescription)")
        })
    }

    func fetch() {
        // fetch data from a data provider
        self.dataProvider.fetch.apply(router).start()

    }
}
