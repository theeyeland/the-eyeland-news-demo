//
//  JLJNObjectMappingAppsModel.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 04.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import Foundation
import ReactiveSwift
import Combine
import Swinject

class BaseObjectMappingAppsModel: Hashable {

    var applicationId: Int = 0
    var title: String = ""
    var subtitle: String = ""
    var router: String = ""
    var imageLink: String = ""

    lazy var assembler: Assembler = Assembler.sharedAssembler

    convenience required init(from decoder: Decoder) throws {
        self.init()
        let values = try decoder.container(keyedBy: CodingKeys.self)
        applicationId = values.decode(.applicationId, withDefault: 0)
        title = values.decode(.title, withDefault: "")
        subtitle = values.decode(.subtitle, withDefault: "")
        router = values.decode(.router, withDefault: "")
        imageLink = values.decode(.imageLink, withDefault: "")
    }

    func hash(into hasher: inout Hasher) {
        hasher.combine(applicationId.hashValue)
    }
}

extension BaseObjectMappingAppsModel: Codable {

    enum CodingKeys: String, CodingKey {
        case applicationId = "application_id"
        case title
        case subtitle
        case router
        case imageLink = "image_link"
    }
}

extension BaseObjectMappingAppsModel {

    static func == (lhs: BaseObjectMappingAppsModel, rhs: BaseObjectMappingAppsModel) -> Bool {
        return lhs.applicationId == rhs.applicationId
    }
}

class ObjectMappingAppsModel: BaseObjectMappingAppsModel {}
