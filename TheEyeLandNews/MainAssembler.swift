//
//  MainAssembler.swift
//  Slovak Lines
//
//  Created by Zoltan Bognar The EyeLand on 03/12/2020.
//  Copyright © 2020 freevision s.r.o. All rights reserved.
//

import Foundation
import Swinject

extension Assembler {

    static let sharedAssembler: Assembler = {
        let container = Container()
        let assembler = Assembler([StoryboardAssembly(withName: TheEyeLandNews.Constants.Storyboard.main),
                                   StoryboardAssembly(withName: TheEyeLandNews.Constants.Storyboard.objMapCombineApps),
                                   StoryboardAssembly(withName: TheEyeLandNews.Constants.Storyboard.objMapSwiftUIView),
                                   DatabaseAssembly(withModelName: TheEyeLandNews.Constants.Database.modelName),
                                   ObjectMappingAppsAssembly(),
                                   AppDelegateAssebly(),
                                   ObjMapCombineAppsAssembly(),
                                   ObjMapSwiftUIViewAssembly(),
                                   RootCoordinatorAssebly(),
                                   RemoteStorageAssembly(),
                                   NetworkingAssembly(),
                                   DBSyncAppsAssembly()], container: container)
        return assembler
    }()
}
