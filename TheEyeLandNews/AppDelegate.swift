//
//  AppDelegate.swift
//  TheEyeLandNews
//
//  Created by Mgr. Art. Ing. Zoltan Bognar on 01.09.17.
//  Copyright © 2020 The EyeLand s.r.o.. All rights reserved.
//

import UIKit
import CoreData

import ReactiveSwift
import Sync
import DATASource

import Swinject
import SwinjectStoryboard
import Firebase

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    lazy var assembler: Assembler = Assembler.sharedAssembler

    func application(_ application: UIApplication,
                     didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.

        SkinStore.sharedInstance.skin = TheEyeLandNewsSkin()

        if let skin = SkinStore.sharedInstance.skin {
            skin.appearance()
        }

        _ = log

        log.outputLevel = .debug
        // Remove comment for view dealloc debug info only for mvvmc objects
        log.theeyeland.deallocFilter(withOtions: DebugDeallocOptions.mvvmc)

        // Remove comment for view dealloc debug info only for viewModels objects
        // log.theeyeland.deallocFilter(withOtions: DeallocDebugOptions.viewModel)

        Networking.initialize()
        FirebaseApp.configure()

        // Get a reference to the storage service using the default Firebase App
//        let storage = Storage.storage()
//
//        // Create a storage reference from our storage service
//        let gsReference = storage.reference() // base path of storage is set in GoogleServiece-Info.plist
//
//        gsReference.child("objectmapping-apps.json").downloadURL { url, error in
//          if let error = error {
//            // Handle any errors
//            print(error)
//          } else {
//            // Get the download URL for 'images/stars.jpg'
//            print(url)
//          }
//        }

        self.window = UIWindow(frame: UIScreen.main.bounds)

        let rootCoordinator = assembler.resolver.resolve(RootCoordinatorProtocol.self)!

        self.window?.rootViewController = rootCoordinator.present()
        self.window?.makeKeyAndVisible()

        return true
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. 
        // This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) 
        // or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. 
        // Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, 
        // and store enough application state information to restore your application 
        // to its current state in case it is terminated later.
        // If your application supports background execution, 
        // this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state;
        // here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. 
        // If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. 
        // See also applicationDidEnterBackground:.
        // Saves changes in the application's managed object context before the application terminates.
    }
}
