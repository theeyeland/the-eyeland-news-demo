# README #

It is only a demonstration app to test various libraries and programming technics

### The Demo App: ###

Every The EyeLand app in the appstore has news list which shows all the other The EyeLand apps beside the current one.
Now the news list is static in the app, so if I want to update the news list I have to create a new build and put it to appstore.

Demo App purpose is to eliminate appstore approval process and enable a live update of news list.

* news list will be downloaded from remote server as json, so I will not need to update the app when I want to change the news list of any app, only the json on the sever
* The demo app can be used for testing of json files before they are used in production
* Main Tabs showing the list of apps in the appstore,
* when you select the row in table view with the app, than the detail view (collection view) shows the news list for the selected app
* when you select one app in the detail view - it sends you to the appstore or to web page when the app is obsolete

Basically it has only 4 tabs : in both tabs you see same data but different technology is used

1. Tab - downloads the json file and it is mapped with codable object mapping and then displayed in table view. Reactive Swift is used for MVVMC
2. Tab - downloads the json file and it is mapped into database with Sync (so after one download it is available offline too )
3. Tab - downloads the json file and it is mapped with codable object mapping and then displayed in table view. Combine Framework is used for MVVMC
4. Tab - Same like in tab 1 but for controller swiftUI is used with UIHostingController. Combine Framework is used for MVVMC. Detail view is reused from tab 3 with UIViewControllerRepresentable

The app is using

* MVVMC architecture (Data Provider, Interactor, Coordinator, ViewModel, View Controller)
* Reactive Swift and Reactive Cocoa
* Combine Framework
* swiftUI
* swinject
* Codable for object mapping
* Sync and DataSource
* Alamofire, AlamofireImage
* Firebase storage
* SwiftLint
* Quick and Nibble for tests,
* I tried to use type erasure for RemoteDataProviderProtocol so I could create dummy remote data provider in Tests
* I created XCGLogger extension to check deallocation of controllers and mvvmc objects


### How do I get set up? ###

* please switch to developer branch and run pod install (tested with pod version 1.10.1)
